<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing');
});
Route::get('/welcome', function () {
    return view('welcome');
});

// Маршруты аутентификации...
Route::get('auth/login', ['as' => 'login', 'uses' => 'Auth\AuthController@showLoginForm']);
Route::post('auth/login', 'Auth\AuthController@login');
Route::get('auth/logout', 'Auth\AuthController@logout');

// Маршруты регистрации...
Route::get('auth/register', 'Auth\RegisterController@showRegistrationForm');
Route::post('auth/register', 'Auth\RegisterController@register');

// Маршруты сброса пароля...
Route::get('password/reset/{token}', 'Admin\UserController@changePasswordForm');
Route::post('password/reset', 'Admin\UserController@changePassword');

Route::group(['middleware' => ['auth', 'role:admin'], 'as' => 'admin'], function () {
    Route::get('home', ['uses' => 'Home\IndexController@index']);

    // Admin panel
    Route::get('/admin/users/list', ['uses' => 'Admin\UserController@getList']);
    Route::get('/admin/users/edit/{id}', ['uses' => 'Admin\UserController@editOne'])->where('id', '[0-9]+');
    Route::post('/admin/users/edit/{id}', ['uses' => 'Admin\UserController@saveOne'])->where('id', '[0-9]+');
    Route::get('/admin/users/change-passwd/{id}', ['uses' => 'Admin\UserController@generateResetToken'])->where('id', '[0-9]+');
    Route::get('/admin/users/block/{id}', ['uses' => 'Admin\UserController@blockOne'])->where('id', '[0-9]+');

    Route::get('/admin/templates/list', ['uses' => 'Admin\TemplatesController@getList']);

    Route::get('/admin/email-templates/list', ['uses' => 'Admin\EmailTemplatesController@getList']);
    Route::get('/admin/email-templates/edit/{id?}', ['uses' => 'Admin\EmailTemplatesController@editOne']);
    Route::post('/admin/email-templates/save/{id?}', ['uses' => 'Admin\EmailTemplatesController@saveOne']);
    Route::get('/admin/email-templates/delete/{id}', ['uses' => 'Admin\EmailTemplatesController@removeOne']);
});

Route::group(['middleware' => ['auth', 'role:user'], 'as' => 'user-access'], function () {
    Route::get('/student/form/{id}', 'Forms\QuestionaireController@getStudentFormReadonly');
    Route::get('/student/cv/{id}', 'Forms\CvController@getStudentFormReadonly');
});

Route::post('/questionaire/student/{guid?}', 'Forms\QuestionaireController@saveStudentForm');
Route::get('/questionaire/student/{guid?}', 'Forms\QuestionaireController@getStudentForm');

Route::post('/cv/{guid}', 'Forms\CvController@saveStudentForm');
Route::get('/cv/{guid}', 'Forms\CvController@getStudentForm');

Route::post('/org/{guid?}', ['middleware' => 'auth', 'uses' => 'Forms\OrgController@saveOrgForm']);
Route::get('/org/{guid?}', ['middleware' => 'auth', 'uses' => 'Forms\OrgController@getOrgForm']);

Route::group(['middleware' => ['auth', 'role:user_manager_admin']], function () {
    // Graduates
    Route::get('/graduates/{year?}/{program?}/{page?}/{sort?}/{direction?}', [
        'as' => 'graduates', 'uses' => 'Graduates\IndexController@getList']);

    Route::get('/search', [
        'as' => 'search', 'uses' => 'Home\SearchController@search']);
    Route::get('/search/custom', [
        'as' => 'search-custom', 'uses' => 'Home\SearchController@searchCustom']);
    Route::get('/student/form/docx/{id}', 'Forms\DocxController@downloadStudentForm');
    Route::get('/student/cv/docx/{guid}', 'Forms\DocxController@downloadStudentCv');

});

Route::group(['middleware' => ['auth', 'role:manager_admin']], function () {
    Route::get('/company/{id}', 'Forms\OrgController@getOrgFormReadonly');
    Route::get('/company/docx/{id}', 'Forms\DocxController@downloadOrgForm');

    // Organizations
    Route::get('/orgs/{type?}/{page?}/{sort?}/{direction?}', [
        'as' => 'ogranizations', 'uses' => 'Organizations\IndexController@getList']);

    Route::get('/graduate/delete/{guid}', [
        'as' => 'graduate-delete', 'uses' => 'Graduates\IndexController@deleteItem']);
    Route::get('/org-delete/{guid}', [
        'as' => 'ogranization-delete', 'uses' => 'Organizations\IndexController@deleteItem']);

    Route::get('/graduate/comment/{id}', [
        'as' => 'graduate-comment-get', 'uses' => 'Graduates\IndexController@commentGet']);
    Route::post('/graduate/comment/{id}', [
        'as' => 'graduate-comment-post', 'uses' => 'Graduates\IndexController@commentEdit']);

    Route::get('/emailing/delivery/start/{id}', ['uses' => 'Emailing\DeliveryController@start']);
    Route::get('/emailing/delivery/stop/{id}', ['uses' => 'Emailing\DeliveryController@stop']);
    Route::get('/emailing/delivery/clear/{id}', ['uses' => 'Emailing\DeliveryController@clear']);
    Route::get('/emailing/delivery/test/{id}', ['uses' => 'Emailing\DeliveryController@test']);

    Route::get('/emailing', ['as' => 'emailing', 'uses' => 'Emailing\DeliveryController@getList']);
    Route::get('/emailing/delivery/edit/{id?}', ['uses' => 'Emailing\DeliveryController@editOne']);
    Route::post('/emailing/delivery/save/{id?}', ['uses' => 'Emailing\DeliveryController@saveOne']);
    Route::get('/emailing/delivery/delete/{id}', ['uses' => 'Emailing\DeliveryController@removeOne']);

    Route::get('/pools', ['as' => 'pools', 'uses' => 'Emailing\PoolController@getList']);
    Route::get('/pools/edit/{pool_id?}', ['uses' => 'Emailing\PoolController@editOne']);
    Route::post('/pools/save/{pool_id?}', ['uses' => 'Emailing\PoolController@saveOne']);
    Route::get('/pools/delete/{pool_id}', ['uses' => 'Emailing\PoolController@removeOne']);

    Route::get('/pools/emails/{pool_id}', ['as' => 'pool_emails', 'uses' => 'Emailing\PoolController@getListOfEmails']);
    Route::get('/pools/emails/add/{pool_id}', ['uses' => 'Emailing\PoolController@emailToPoolForm']);
    Route::post('/pools/emails/add/{pool_id}', ['uses' => 'Emailing\PoolController@addEmailToPool']);
    Route::get('/pools/emails/delete/{id}', ['uses' => 'Emailing\PoolController@removeEmailFromPool']);
});
