<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"
                xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
                xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                xmlns:o="urn:schemas-microsoft-com:office:office"
                xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                xmlns:v="urn:schemas-microsoft-com:vml"
                xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
                xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
                xmlns:w10="urn:schemas-microsoft-com:office:word"
                xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"
                xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk"
                xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
                xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape">
    <xsl:output method="xml"  indent="yes"/>
    <xsl:strip-space elements="*"/>

    <xsl:template match="/form">
        <w:document mc:Ignorable="w14 wp14">
            <w:body>
                <w:p w:rsidR="006D62AB" w:rsidRDefault="00680007" w:rsidP="00680007">
                    <w:pPr>
                        <w:pStyle w:val="a3"/>
                    </w:pPr>
                    <w:r>
                        <w:t>Анкета выпускника</w:t>
                    </w:r>
                </w:p>
                <xsl:apply-templates select="item" />
            </w:body>
        </w:document>
    </xsl:template>


    <xsl:template match="item" >
        <w:p w:rsidR="00680007" w:rsidRDefault="00680007" w:rsidP="00680007">
            <w:r>
                <w:rPr><w:b/></w:rPr>
                <w:t><xsl:value-of select="question/text()"/></w:t>
            </w:r>
        </w:p>
        <xsl:apply-templates select="input"/>
        <xsl:apply-templates select="textarea"/>
        <xsl:apply-templates select="radio_answer"/>
        <xsl:apply-templates select="checkbox_answer"/>
        <!--xsl:apply-templates select="checkbox_text_answer"/-->
        <xsl:apply-templates select="comment"/>
    </xsl:template>


    <xsl:template match="input" >
        <w:p w:rsidR="00680007" w:rsidRPr="00680007" w:rsidRDefault="00680007" w:rsidP="00680007">
            <w:r>
                <w:t><xsl:value-of select="text()"/></w:t>
            </w:r>
            <w:bookmarkStart w:id="0" w:name="_GoBack"/>
            <w:bookmarkEnd w:id="0"/>
        </w:p>
    </xsl:template>

    <xsl:template match="textarea">
        <w:p w:rsidR="00680007" w:rsidRPr="00680007" w:rsidRDefault="00680007" w:rsidP="00680007">
            <w:r>
                <w:t><xsl:value-of select="text()"/></w:t>
            </w:r>
            <w:bookmarkStart w:id="0" w:name="_GoBack"/>
            <w:bookmarkEnd w:id="0"/>
        </w:p>
    </xsl:template>

    <xsl:template match="radio_answer" >
        <xsl:apply-templates select="item" mode="Radio"/>
    </xsl:template>
    <xsl:template match="item" mode="Radio">
        <xsl:if test="radio/@checked = 'true'">
            <w:p w:rsidR="00680007" w:rsidRPr="00680007" w:rsidRDefault="00680007" w:rsidP="00680007">
                <w:r>
                    <w:t><xsl:value-of select="radio/text()"/></w:t>
                </w:r>
                <w:bookmarkStart w:id="0" w:name="_GoBack"/>
                <w:bookmarkEnd w:id="0"/>
            </w:p>
        </xsl:if>
     </xsl:template>

    <xsl:template match="checkbox_answer" >
        <xsl:apply-templates select="item" mode="Checkbox"/>
    </xsl:template>
    <xsl:template match="item" mode="Checkbox">
        <xsl:if test="checkbox/@checked = 'true'">
            <w:p w:rsidR="00680007" w:rsidRPr="00680007" w:rsidRDefault="00680007" w:rsidP="00680007">
                <w:r>
                    <w:t><xsl:text>- </xsl:text><xsl:value-of select="checkbox/text()"/></w:t>
                </w:r>
                <w:bookmarkStart w:id="0" w:name="_GoBack"/>
                <w:bookmarkEnd w:id="0"/>
            </w:p>
        </xsl:if>
    </xsl:template>

    <xsl:template match="checkbox_text_answer" >
        <xsl:apply-templates select="item" mode="CheckboxWithText"/>
    </xsl:template>
    <xsl:template match="item" mode="CheckboxWithText">

        <xsl:if test="checkbox/@checked = 'true'">
            <w:p w:rsidR="00680007" w:rsidRPr="00680007" w:rsidRDefault="00680007" w:rsidP="00680007">
                <w:r>
                    <w:t>
                        <xsl:value-of select="checkbox/@value"/>
                        <xsl:if test="input">
                            <xsl:text>:&#160;&#160;</xsl:text>
                        </xsl:if>
                    </w:t>
                </w:r>
                <xsl:if test="input">
                    <w:r>
                        <w:rPr><w:i/></w:rPr>
                        <w:t><xsl:value-of select="input/text()"/></w:t>
                    </w:r>
                </xsl:if>
                <w:bookmarkStart w:id="0" w:name="_GoBack"/>
                <w:bookmarkEnd w:id="0"/>
            </w:p>
        </xsl:if>
    </xsl:template>

    <xsl:template match="comment">
        <xsl:if test="string-length(input/text()) > 0">
            <w:p w:rsidR="00680007" w:rsidRPr="00680007" w:rsidRDefault="00680007" w:rsidP="00680007">
                <w:r>
                    <w:t><xsl:value-of select="title/text()"/><xsl:text>:&#160;&#160;</xsl:text></w:t>
                </w:r>
                <w:r>
                    <w:rPr><w:i/></w:rPr>
                    <w:t><xsl:value-of select="input/text()"/></w:t>
                </w:r>
                <w:bookmarkStart w:id="0" w:name="_GoBack"/>
                <w:bookmarkEnd w:id="0"/>
            </w:p>
        </xsl:if>
    </xsl:template>


</xsl:stylesheet>