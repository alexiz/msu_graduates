<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/">
    <xsl:output method="html"  indent="yes"/>
    <xsl:strip-space elements="*"/>

    <xsl:template match="/form">
        <xsl:apply-templates select="item" />
    </xsl:template>


    <xsl:template match="item" >
        <div class="form-group">
            <label for="inputText3"><xsl:value-of select="question/text()"/></label>
            <xsl:apply-templates select="input"/>
            <xsl:apply-templates select="textarea"/>
            <xsl:apply-templates select="radio_answer"/>
            <xsl:apply-templates select="checkbox_answer"/>
            <xsl:apply-templates select="checkbox_text_answer"/>
            <xsl:apply-templates select="comment"/>
        </div>
    </xsl:template>


    <xsl:template match="input" >
        <xsl:if test="@required = 'true'">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-asterisk"></i></span>
                <input type="text" class="form-control" placeholder="">
                    <xsl:attribute name="name">
                        <!--xsl:if test="@db = 'true'">
                            <xsl:text>db_</xsl:text>
                        </xsl:if-->
                        <xsl:value-of select="@field"/>
                    </xsl:attribute>
                    <xsl:attribute name="value">
                        <xsl:value-of select="text()"/>
                    </xsl:attribute>
                </input>
            </div>
        </xsl:if>
        <xsl:if test="@required = 'false'">
            <input type="text" class="form-control" placeholder="">
                <xsl:attribute name="name">
                    <!--xsl:if test="@db = 'true'">
                        <xsl:text>db_</xsl:text>
                    </xsl:if-->
                    <xsl:value-of select="@field"/>
                </xsl:attribute>
                <xsl:attribute name="value">
                    <xsl:value-of select="text()"/>
                </xsl:attribute>
            </input>
        </xsl:if>
    </xsl:template>

    <xsl:template match="textarea">
        <textarea class="form-control" rows="15">
            <xsl:attribute name="name">
                <xsl:value-of select="@field"/>
            </xsl:attribute>
            <xsl:value-of select="text()"/>
        </textarea>
    </xsl:template>

    <xsl:template match="radio_answer" >
        <xsl:apply-templates select="item" mode="Radio"/>
    </xsl:template>
    <xsl:template match="item" mode="Radio">
        <div class="radio">
            <label>
                <input type="radio">
                    <xsl:attribute name="name">
                        <!--xsl:if test="radio/@db = 'true'">
                            <xsl:text>db_</xsl:text>
                        </xsl:if-->
                        <xsl:value-of select="radio/@field"/>
                    </xsl:attribute>
                    <xsl:attribute name="value">
                        <xsl:value-of select="radio/@value"/>
                    </xsl:attribute>
                    <xsl:if test="radio/@checked = 'true'">
                        <xsl:attribute name="checked"><xsl:text>true</xsl:text></xsl:attribute>
                    </xsl:if>
                </input>

                <xsl:value-of select="radio/text()"/>
            </label>
        </div>
    </xsl:template>

    <xsl:template match="checkbox_answer" >
        <xsl:apply-templates select="item" mode="Checkbox"/>
    </xsl:template>
    <xsl:template match="item" mode="Checkbox">
        <div class="checkbox">
            <label>
                <input type="checkbox">
                    <xsl:attribute name="name">
                        <!--xsl:if test="checkbox/@db = 'true'">
                            <xsl:text>db_</xsl:text>
                        </xsl:if-->
                        <xsl:value-of select="checkbox/@field"/>
                    </xsl:attribute>
                    <xsl:attribute name="value">
                        <xsl:value-of select="checkbox/@value"/>
                    </xsl:attribute>
                    <xsl:if test="checkbox/@checked = 'true'">
                        <xsl:attribute name="checked"><xsl:text>true</xsl:text></xsl:attribute>
                    </xsl:if>
                </input>

                <xsl:value-of select="checkbox/text()"/>
            </label>
        </div>
    </xsl:template>

    <xsl:template match="checkbox_text_answer" >
        <xsl:apply-templates select="item" mode="CheckboxWithText"/>
    </xsl:template>
    <xsl:template match="item" mode="CheckboxWithText">
        <div class="checkbox">
            <label>
                <input type="checkbox">
                    <xsl:attribute name="name">
                        <!--xsl:if test="checkbox/@db = 'true'">
                            <xsl:text>db_</xsl:text>
                        </xsl:if-->
                        <xsl:value-of select="checkbox/@field"/>
                    </xsl:attribute>
                    <xsl:attribute name="value">
                        <xsl:value-of select="checkbox/@value"/>
                    </xsl:attribute>
                    <xsl:if test="checkbox/@checked = 'true'">
                        <xsl:attribute name="checked"><xsl:text>true</xsl:text></xsl:attribute>
                    </xsl:if>
                </input>

                <xsl:value-of select="checkbox/text()"/>

                <xsl:if test="input">
                    <input type="text" class="form-control">
                        <xsl:attribute name="placeholder">
                            <xsl:value-of select="subtitle/text()"/>
                        </xsl:attribute>
                        <xsl:attribute name="name">
                            <!--xsl:if test="input/@db = 'true'">
                                <xsl:text>db_</xsl:text>
                            </xsl:if-->
                            <xsl:value-of select="input/@field"/>
                        </xsl:attribute>
                        <xsl:attribute name="value">
                            <xsl:value-of select="input/text()"/>
                        </xsl:attribute>
                    </input>
                </xsl:if>
            </label>
        </div>
    </xsl:template>

    <xsl:template match="comment">
        <xsl:if test="title">
            <label>
                <xsl:value-of select="title/text()"/>
            </label>
        </xsl:if>
        <textarea class="form-control" rows="2">
            <xsl:attribute name="name">
                <!--xsl:if test="input/@db = 'true'">
                    <xsl:text>db_</xsl:text>
                </xsl:if-->
                <xsl:value-of select="input/@field"/>
            </xsl:attribute>
            <xsl:attribute name="placeholder">
                <xsl:value-of select="subtitle/text()"/>
            </xsl:attribute>
            <xsl:value-of select="input/text()"/>
        </textarea>
    </xsl:template>


</xsl:stylesheet>