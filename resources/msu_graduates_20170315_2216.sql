﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 7.2.53.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 15.03.2017 22:16:32
-- Версия сервера: 5.5.5-10.0.17-MariaDB
-- Версия клиента: 4.1
--


-- 
-- Отключение внешних ключей
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Установить режим SQL (SQL mode)
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

-- 
-- Установка базы данных по умолчанию
--
USE msu_graduates;

--
-- Описание для таблицы deliveries
--
DROP TABLE IF EXISTS deliveries;
CREATE TABLE deliveries (
  id INT(11) NOT NULL AUTO_INCREMENT,
  subject VARCHAR(255) DEFAULT NULL,
  email_template_id VARCHAR(255) DEFAULT NULL,
  test_recepient_email VARCHAR(255) DEFAULT NULL,
  test_recepient_name VARCHAR(255) DEFAULT NULL,
  attachment VARCHAR(255) DEFAULT NULL,
  is_active TINYINT(1) DEFAULT NULL,
  created DATETIME DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы delivery_queue
--
DROP TABLE IF EXISTS delivery_queue;
CREATE TABLE delivery_queue (
  job_id INT(11) DEFAULT NULL,
  delivery_id INT(11) DEFAULT NULL,
  pause TINYINT(1) DEFAULT NULL,
  is_busy TINYINT(1) DEFAULT NULL,
  recepient_email VARCHAR(255) DEFAULT NULL,
  recepient_name VARCHAR(255) DEFAULT NULL
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы email_pools
--
DROP TABLE IF EXISTS email_pools;
CREATE TABLE email_pools (
  id INT(11) NOT NULL AUTO_INCREMENT,
  pool_name VARCHAR(255) DEFAULT NULL,
  recepient_email VARCHAR(255) DEFAULT NULL,
  recepient_name VARCHAR(255) DEFAULT NULL,
  is_accessable_personal_data TINYINT(1) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы email_templates
--
DROP TABLE IF EXISTS email_templates;
CREATE TABLE email_templates (
  id INT(11) NOT NULL AUTO_INCREMENT,
  body TEXT DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы employees
--
DROP TABLE IF EXISTS employees;
CREATE TABLE employees (
  id INT(11) NOT NULL AUTO_INCREMENT,
  organization_id INT(11) DEFAULT NULL,
  name VARCHAR(500) DEFAULT NULL,
  phone INT(11) DEFAULT NULL,
  email VARCHAR(50) DEFAULT NULL,
  `position` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы form_templates
--
DROP TABLE IF EXISTS form_templates;
CREATE TABLE form_templates (
  id INT(11) NOT NULL AUTO_INCREMENT,
  template TEXT DEFAULT NULL,
  type VARCHAR(50) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX UK_form_templates_type (type)
)
ENGINE = INNODB
AUTO_INCREMENT = 4
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы graduates
--
DROP TABLE IF EXISTS graduates;
CREATE TABLE graduates (
  id INT(11) NOT NULL AUTO_INCREMENT,
  form_id INT(11) DEFAULT NULL,
  graduation_year INT(11) DEFAULT NULL,
  student_id INT(11) DEFAULT NULL,
  name VARCHAR(500) DEFAULT NULL,
  email VARCHAR(255) DEFAULT NULL,
  phone VARCHAR(20) DEFAULT NULL,
  group_number VARBINARY(20) DEFAULT NULL,
  program VARCHAR(50) DEFAULT NULL,
  current_employer_name VARCHAR(500) DEFAULT NULL,
  citizenship VARCHAR(50) DEFAULT NULL,
  is_accessable_personal_data TINYINT(1) DEFAULT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  form_template_id INT(11) DEFAULT NULL,
  guid VARCHAR(32) DEFAULT NULL,
  filepath VARCHAR(255) DEFAULT NULL,
  skills TEXT DEFAULT NULL,
  is_looking_for_work INT(11) DEFAULT NULL,
  PRIMARY KEY (id),
  INDEX IDX_graduates_guid (guid)
)
ENGINE = INNODB
AUTO_INCREMENT = 14
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы organizations
--
DROP TABLE IF EXISTS organizations;
CREATE TABLE organizations (
  id INT(11) NOT NULL AUTO_INCREMENT,
  form_id INT(11) DEFAULT NULL,
  org_type VARCHAR(50) DEFAULT NULL,
  name VARCHAR(500) DEFAULT NULL,
  phone INT(11) DEFAULT NULL,
  address VARCHAR(500) DEFAULT NULL,
  email VARCHAR(50) DEFAULT NULL,
  employee_name VARCHAR(255) DEFAULT NULL,
  employee_phone VARCHAR(255) DEFAULT NULL,
  employee_email VARCHAR(255) DEFAULT NULL,
  employee_position VARCHAR(255) DEFAULT NULL,
  comment VARCHAR(500) DEFAULT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  form_template_id INT(11) DEFAULT NULL,
  guid VARCHAR(32) DEFAULT NULL,
  filepath VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы roles
--
DROP TABLE IF EXISTS roles;
CREATE TABLE roles (
  id INT(11) NOT NULL AUTO_INCREMENT,
  role VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 5
AVG_ROW_LENGTH = 4096
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы users
--
DROP TABLE IF EXISTS users;
CREATE TABLE users (
  id INT(11) NOT NULL DEFAULT 0,
  email VARCHAR(50) NOT NULL,
  password VARCHAR(60) NOT NULL,
  role_id INT(11) NOT NULL DEFAULT 1,
  working_till DATETIME DEFAULT NULL,
  is_blocked TINYINT(1) NOT NULL DEFAULT 1,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  remember_token VARCHAR(100) DEFAULT NULL,
  name VARCHAR(255) NOT NULL,
  PRIMARY KEY (email),
  UNIQUE INDEX UK_users_email (email)
)
ENGINE = INNODB
AVG_ROW_LENGTH = 16384
CHARACTER SET utf8
COLLATE utf8_general_ci;

-- 
-- Вывод данных для таблицы deliveries
--

-- Таблица msu_graduates.deliveries не содержит данных

-- 
-- Вывод данных для таблицы delivery_queue
--

-- Таблица msu_graduates.delivery_queue не содержит данных

-- 
-- Вывод данных для таблицы email_pools
--

-- Таблица msu_graduates.email_pools не содержит данных

-- 
-- Вывод данных для таблицы email_templates
--

-- Таблица msu_graduates.email_templates не содержит данных

-- 
-- Вывод данных для таблицы employees
--

-- Таблица msu_graduates.employees не содержит данных

-- 
-- Вывод данных для таблицы form_templates
--
INSERT INTO form_templates VALUES
(1, 'resources/templates/form.xml', 'main'),
(3, 'resources/templates/cv.xml', 'cv');

-- 
-- Вывод данных для таблицы graduates
--

-- Таблица msu_graduates.graduates не содержит данных

-- 
-- Вывод данных для таблицы organizations
--

-- Таблица msu_graduates.organizations не содержит данных

-- 
-- Вывод данных для таблицы roles
--
INSERT INTO roles VALUES
(1, 'Студент'),
(2, 'Пользователь'),
(3, 'Менеджер'),
(4, 'Администратор');

-- 
-- Вывод данных для таблицы users
--
INSERT INTO users VALUES
(1, 'test@test.com', '$2y$10$h/Sgv/dGFGBIY08JspJyBeZLpc66UWOZ1OqC9RPhGqzXtB0WC5Ovm', 1, NULL, 1, '2017-02-23 16:41:27', '2017-03-12 13:39:54', '', 'Mark');

-- 
-- Восстановить предыдущий режим SQL (SQL mode)
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Включение внешних ключей
-- 
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;