@extends('layouts.user')

@section('content')

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <!--li class="header">MAIN NAVIGATION</li-->
                @php $total = 0; @endphp
                @foreach ($tree as $key => $value)
                    <li class="treeview @if( Request::route('year') == $key ) active @endif">
                        <a href="#"><i class="fa fa-graduation-cap"></i> <span>Выпуск {{ $key }}</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            @php $total_for_all_programs = 0 @endphp
                            @foreach ($value as $item)
                                <li @if( Request::route('year') == $key && Request::route('program') == $item['program'] ) class="active" @endif><a href="/graduates/{{ $key }}/{{ $item['program'] }}"><i class="fa fa-circle-o"></i> {{ $item['label'] }} ({{ $item['total'] }})</a></li>
                                @php $total_for_all_programs += $item['total'] @endphp
                                @php if( Request::route('year') == $key && Request::route('program') == $item['program'] ) $total = $item['total']; @endphp
                            @endforeach
                            <li @if( Request::route('year') == $key && (Request::route('program') == 'all' || empty(Request::route('program'))) ) class="active" @endif><a href="/graduates/{{ $key }}"><i class="fa fa-circle-o"></i> Все программы ({{ $total_for_all_programs }})</a></li>
                            @php if( Request::route('year') == $key && $total == 0 ) $total = $total_for_all_programs; @endphp
                        </ul>
                    </li>
                @endforeach
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <section class="content">
            <div class="row">
                <div class="col-xs-12">


                    <div class="box">
                        <div class="box-header">
                            @php if( !is_null($program_title) ) $program_title = ', ' . $program_title @endphp
                            <h3 class="box-title">Выпуск {{ $year_title }}{{ $program_title  }}</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th><a href="{{ URL::action('Graduates\IndexController@getList', ['year' => empty(Request::route('year')) ? date("Y") : (int)Request::route('year'),
                                                                                                        'program' => empty(Request::route('program')) ? 'all' : Request::route('program'),
                                                                                                        'page' => empty(Request::route('page')) ? '1' : Request::route('page'),
                                                                                                        'sort' => 'graduation_year',
                                                                                                        'direction' => Request::route('direction') == 'asc' ? 'desc' : 'asc']) }}">Выпуск (год)</a> @include('helpers.sort', ['column' => 'graduation_year'])</th>
                                    <th><a href="{{ URL::action('Graduates\IndexController@getList', ['year' => empty(Request::route('year')) ? date("Y") : (int)Request::route('year'),
                                                                                                        'program' => empty(Request::route('program')) ? 'all' : Request::route('program'),
                                                                                                        'page' => empty(Request::route('page')) ? '1' : Request::route('page'),
                                                                                                        'sort' => 'student_id',
                                                                                                        'direction' => Request::route('direction') == 'asc' ? 'desc' : 'asc']) }}">№ студ. билета</a> @include('helpers.sort', ['column' => 'student_id'])</th>
                                    <th><a href="{{ URL::action('Graduates\IndexController@getList', ['year' => empty(Request::route('year')) ? date("Y") : (int)Request::route('year'),
                                                                                                        'program' => empty(Request::route('program')) ? 'all' : Request::route('program'),
                                                                                                        'page' => empty(Request::route('page')) ? '1' : Request::route('page'),
                                                                                                        'sort' => 'name',
                                                                                                        'direction' => Request::route('direction') == 'asc' ? 'desc' : 'asc']) }}">ФИО</a> @include('helpers.sort', ['column' => 'name'])</th>
                                    <th><a href="{{ URL::action('Graduates\IndexController@getList', ['year' => empty(Request::route('year')) ? date("Y") : (int)Request::route('year'),
                                                                                                        'program' => empty(Request::route('program')) ? 'all' : Request::route('program'),
                                                                                                        'page' => empty(Request::route('page')) ? '1' : Request::route('page'),
                                                                                                        'sort' => 'email',
                                                                                                        'direction' => Request::route('direction') == 'asc' ? 'desc' : 'asc']) }}">Email</a> @include('helpers.sort', ['column' => 'email'])</th>
                                    <th><a href="{{ URL::action('Graduates\IndexController@getList', ['year' => empty(Request::route('year')) ? date("Y") : (int)Request::route('year'),
                                                                                                        'program' => empty(Request::route('program')) ? 'all' : Request::route('program'),
                                                                                                        'page' => empty(Request::route('page')) ? '1' : Request::route('page'),
                                                                                                        'sort' => 'phone',
                                                                                                        'direction' => Request::route('direction') == 'asc' ? 'desc' : 'asc']) }}">Телефон</a> @include('helpers.sort', ['column' => 'phone'])</th>
                                    <th>Гражданство</th>
                                    <th>Ищу/не ищу работу</th>
                                    <!--th>Комментарий</th-->
                                    <th>Анкета</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach ($graduates as $graduate)
                                    <tr>
                                        <td>{{ $graduate->graduation_year  }}</td>
                                        <td>{{ $graduate->student_id  }}</td>
                                        <td>{{ $graduate->name  }}</td>
                                        <td>{{ $graduate->email  }}</td>
                                        <td>{{ $graduate->phone  }}</td>
                                        @php $citizenship = 'Иное'; if( $graduate->citizenship == 'rf' ) $citizenship = 'РФ'; @endphp
                                        <td>{{ $citizenship  }}</td>
                                        <td>{{ $graduate->is_looking_for_work  }}</td>
                                        <!--td></td-->
                                        <td><a href="/student/form/{{ $graduate->id  }}" target="_blank" class="btn btn-block btn-primary">Анкета</a></td>
                                    </tr>
                                @endforeach

                                </tbody>
                                <!--tfoot>
                                <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                                </tr>
                                </tfoot-->
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <ul class="pagination pagination-sm no-margin pull-right">
                                <li class="paginate_button"><a href="{{ URL::action('Graduates\IndexController@getList', ['year' => empty(Request::route('year')) ? date("Y") : (int)Request::route('year'),
                                                                                                        'program' => empty(Request::route('program')) ? 'all' : Request::route('program'),
                                                                                                        'page' => 1,
                                                                                                        'sort' => Request::route('sort'),
                                                                                                        'direction' => Request::route('direction')]) }}">&laquo;</a></li>
                                @php $count = ceil($total / $limit) @endphp
                                @for($i = 1; $i < $count + 1; $i++)
                                    @php $active = ''; if( $i == Request::route('page') ) $active = 'active'; @endphp
                                    <li class="paginate_button {{ $active }}"><a href="{{ URL::action('Graduates\IndexController@getList', ['year' => empty(Request::route('year')) ? date("Y") : (int)Request::route('year'),
                                                                                                        'program' => empty(Request::route('program')) ? 'all' : Request::route('program'),
                                                                                                        'page' => $i,
                                                                                                        'sort' => Request::route('sort'),
                                                                                                        'direction' => Request::route('direction')]) }}">{{ $i  }}</a></li>
                                @endfor
                                <li class="paginate_button"><a href="{{ URL::action('Graduates\IndexController@getList', ['year' => empty(Request::route('year')) ? date("Y") : (int)Request::route('year'),
                                                                                                        'program' => empty(Request::route('program')) ? 'all' : Request::route('program'),
                                                                                                        'page' => $count,
                                                                                                        'sort' => Request::route('sort'),
                                                                                                        'direction' => Request::route('direction')]) }}">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.box -->

                </div>
            </div>
        </section>
    </div>
    <!-- /.content-wrapper -->

@endsection