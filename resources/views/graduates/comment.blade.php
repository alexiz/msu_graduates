@extends('layouts.manager')

@section('content')



    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <!--li class="header">MAIN NAVIGATION</li-->
                @php $total = 0; @endphp
                @foreach ($tree as $key => $value)
                    <li class="treeview">
                        <a href="#"><i class="fa fa-graduation-cap"></i> <span>Выпуск {{ $key }}</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            @php $total_for_all_programs = 0 @endphp
                            @foreach ($value as $item)
                                <li><a href="/graduates/{{ $key }}/{{ $item['program'] }}"><i class="fa fa-circle-o"></i> {{ $item['label'] }} ({{ $item['total'] }})</a></li>
                                @php $total_for_all_programs += $item['total'] @endphp
                            @endforeach
                            <li><a href="/graduates/{{ $key }}"><i class="fa fa-circle-o"></i> Все программы ({{ $total_for_all_programs }})</a></li>
                        </ul>
                    </li>
                @endforeach
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- =============================================== -->


    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">

                <!-- left column -->
                <div class="col-md-12">
                    @if( count($errors->all()) > 0 )
                        <div class="callout callout-danger">
                            <h4>Ошибки ввода</h4>
                            @foreach ($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                @endif

                <!-- Horizontal Form -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Выпускник {{ $graduate->name }}</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form class="form-horizontal" method="POST" action="/graduate/comment/{{ $graduate->id }}">
                            {!! csrf_field() !!}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="recepient_name" class="col-sm-2 control-label">Комментарий к анкете</label>

                                    <div class="col-sm-10">
                                        <textarea name="comment" rows="10" cols="80">{{ isset($graduate->comment) ? $graduate->comment : '' }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Сохранить</button>
                                <a href="/graduates" class="btn btn-info pull-right" style="margin-right:20px;margin-left:20px;">Назад к списку</a>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                    <!-- /.box -->

                </div>
                <!--/.col (left) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

@endsection