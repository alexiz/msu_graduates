@extends('layouts.register')

@section('content')

  <div class="register-box-body">
    <p class="login-box-msg">Вход</p>

    <form action="/auth/login" method="POST">
      {!! csrf_field() !!}
      <div class="form-group has-feedback @if( count($errors->all()) > 0 ) {!! "has-error" !!} @endif">
        <input name="email" type="email" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback @if( count($errors->all()) > 0 ) {!! "has-error" !!} @endif">
        <input name="password" type="password" class="form-control" placeholder="Пароль">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Войти</button>
        </div>
      </div>
    </form>
  </div>

@endsection