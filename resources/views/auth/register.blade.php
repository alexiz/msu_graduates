@extends('layouts.register')

@section('content')

    <div class="register-box-body">
        <p class="login-box-msg">Регистрация</p>

        @if( count($errors->all()) > 0 )
            <div class="callout callout-danger">
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        @endif
        <form action="/auth/register" method="POST">
            {!! csrf_field() !!}
            <div class="form-group has-feedback">
                <input name="name" type="text" class="form-control" placeholder="Имя">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input name="email" type="email" class="form-control" placeholder="Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input name="password" type="password" class="form-control" placeholder="Пароль">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input name="password_confirmation" type="password" class="form-control" placeholder="Подтверждение пароля">
                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
            </div>
            <div class="row">
                <!--div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox"> I agree to the <a href="#">terms</a>
                        </label>
                    </div>
                </div-->
                <!-- /.col -->
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Зарегистрироваться</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

        <!--div class="social-auth-links text-center">
            <p>- OR -</p>
            <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using
                Facebook</a>
            <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using
                Google+</a>
        </div>

        <a href="login.html" class="text-center">I already have a membership</a-->
    </div>

    <!-- Main content -->
    <!--section class="content">
        <div class="row">

            <div class="col-md-6">


                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Регистрация</h3>
                    </div>

                    <form class="form-horizontal" method="POST" action="/auth/register">
                        {!! csrf_field() !!}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputText3" class="col-sm-2 control-label">Name</label>

                                <div class="col-sm-10">
                                    <input name="name" type="text" class="form-control" id="inputText3" placeholder="Name" value="{{ old('name') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Email</label>

                                <div class="col-sm-10">
                                    <input name="email" type="email" class="form-control" id="inputEmail3" placeholder="Email" value="{{ old('email') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Password</label>

                                <div class="col-sm-10">
                                    <input name="password" type="password" class="form-control" id="inputPassword3" placeholder="Password">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputRepeatPassword3" class="col-sm-2 control-label">Confirm Password</label>

                                <div class="col-sm-10">
                                    <input name="password_confirmation" type="password" class="form-control" id="inputRepeatPassword3" placeholder="Confirm password">
                                </div>
                            </div>
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right">Зарегистрироваться</button>
                        </div>

                    </form>
                </div>


            </div>

            <div class="col-md-6">

            </div>

        </div>

    </section-->


@endsection