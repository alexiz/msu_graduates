@extends('layouts.main')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Email шаблоны</h3>
                        <div class="box-tools"><a href="/admin/email-templates/edit" class="btn btn-block btn-success">Добавить новый</a></div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Шаблон</th>
                                <th>Редактировать</th>
                                <th>Удалить</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($dataset as $item)
                                <tr>
                                    <td>Шаблон {{ $item->id }}</td>
                                    <td width="100px"><a href="/admin/email-templates/edit/{{ $item->id }}" class="btn btn-block btn-primary">Редактировать</a></td>
                                    <td width="100px"><a href="/admin/email-templates/delete/{{ $item->id }}" class="btn btn-block btn-danger">Удалить</a></td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

@endsection