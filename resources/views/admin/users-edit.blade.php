@extends('layouts.main')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">

            <!-- left column -->
            <div class="col-md-12">
                @if( count($errors->all()) > 0 )
                    <div class="callout callout-danger">
                        <h4>Ошибки ввода</h4>
                        @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif

                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Редактировать пользователя</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="POST" action="/admin/users/edit/{{ $user->id }}">
                        {!! csrf_field() !!}
                        <input type="hidden" name="id" value="{{ $user->id }}">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputText3" class="col-sm-2 control-label">Имя</label>

                                <div class="col-sm-10">
                                    <input name="name" type="text" class="form-control" id="inputText3" placeholder="Name" value="{{ $user->name }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Email</label>

                                <div class="col-sm-10">
                                    <input name="email" type="email" class="form-control" id="inputEmail3" placeholder="Email" value="{{ $user->email }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputRole3" class="col-sm-2 control-label">Роль</label>

                                <div class="col-sm-10">
                                    <select class="form-control" name="role_id" id="inputRole3">
                                        @foreach ($roles as $role)
                                            <option value="{{ $role->id }}" {{ $user->role->id == $role->id ? 'selected' : '' }}>{{ $role->role }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right">Сохранить</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->

            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

@endsection