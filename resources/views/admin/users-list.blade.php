@extends('layouts.main')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">

                <div class="box">
    <div class="box-header">
        <h3 class="box-title">Пользователи системы</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="example2" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>Id</th>
                <th>Имя</th>
                <th>Email</th>
                <th>Роль</th>
                <th>Статус</th>
                <!--th>Активен до</th-->
                <th>Редактировать</th>
                <th>Сменить пароль</th>
                <th>Заблокировать</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->role->role }}</td>
                    <td>
                        @if( $user->is_blocked == 1 )
                            Заблокирован
                        @else
                            Активен
                        @endif
                    </td>
                    <!--td>{{ $user->working_till }}</td-->
                    <td><a href="/admin/users/edit/{{ $user->id }}" class="btn btn-block btn-primary">Редактировать</a></td>
                    <td><a href="/admin/users/change-passwd/{{ $user->id }}" class="btn btn-block btn-warning">Сменить пароль</a></td>
                    <td>
                        @if( $user->is_blocked == 1 )
                            <a href="/admin/users/block/{{ $user->id }}" class="btn btn-block btn-success">Активировать</a>
                        @else
                            <a href="/admin/users/block/{{ $user->id }}" class="btn btn-block btn-danger">Блокировать</a>
                        @endif
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

@endsection