@extends('layouts.main')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">

            <!-- left column -->
            <div class="col-md-12">
                @if( count($errors->all()) > 0 )
                    <div class="callout callout-danger">
                        <h4>Ошибки ввода</h4>
                        @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
            @endif

            <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Шаблон письма</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="POST" action="/admin/email-templates/save{{ isset($item->id) ? '/' . $item->id : '' }}">
                        {!! csrf_field() !!}
                        <div class="box-body">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <textarea id="editor1" name="body" rows="10" cols="80">{{ isset($item->body) ? $item->body : '' }}</textarea>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right">Сохранить</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->

            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

@endsection