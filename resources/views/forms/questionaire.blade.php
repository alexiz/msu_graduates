@extends('layouts.form')

@section('content')

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Анкета выпускника</h3>
                    </div>
                    @if( count($errors->all()) > 0 )
                        <div class="callout callout-danger">
                            <h4>Ошибки ввода</h4>
                            @foreach ($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="box-body" method="POST" action="/questionaire/student/{{ $guid }}">
                        {!! csrf_field() !!}
                        <input type="hidden" name="id" value="">
                        <div class="box-body">
                            {!! $form !!}
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right">Сохранить</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->

            </div>
        </div>
    </section>

@endsection