@extends('layouts.form-readonly')

@section('content')

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Резюме</h3>
                    </div>
                <!-- /.box-header -->

                        <div class="box-body">
                            {!! $form !!}
                        </div>

                </div>
                <!-- /.box -->

            </div>
        </div>
    </section>

@endsection