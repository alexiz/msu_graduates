@extends('layouts.org')

@section('content')

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Анкета работодателя</h3>
                    </div>
                <!-- /.box-header -->
                    <!-- form start -->

                        <div class="box-body">
                            {!! $form !!}
                        </div>

                </div>
                <!-- /.box -->

            </div>
        </div>
    </section>

@endsection