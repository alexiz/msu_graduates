@extends('layouts.manager')

@section('content')

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <!--li class="header">MAIN NAVIGATION</li-->
                @php $total = 0; @endphp
                @foreach ($types as $type_item)
                    <li class="treeview @if( Request::route('type') == $type_item->org_type ) active @endif">
                        <a href="/orgs/{{ $type_item->org_type  }}"><i class="fa fa-cubes"></i> <span>{{ $labels[$type_item->org_type]['short'] }} ({{ $type_item->total }})</span></a>
                        @php $total += $type_item->total @endphp
                    </li>
                @endforeach

                <li class="treeview @if(Request::route('type') == 'all' || empty(Request::route('type')) ) active @endif" >
                    <a href="/orgs/all"><i class="fa fa-cubes"></i> <span>Все ({{ $total  }})</span></a>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <section class="content">
            <div class="row">
                <div class="col-xs-12">


                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">{{ $org_type_title['long'] }}</h3>
                            <div class="box-tools"><a href="/org" target="_blank" class="btn btn-block btn-success">Добавить новую организацию</a></div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Тип</th>
                                    <th><a href="{{ URL::action('Organizations\IndexController@getList', [
                                                                                                        'type' => empty(Request::route('type')) ? 'all' : Request::route('type'),
                                                                                                        'page' => empty(Request::route('page')) ? '1' : Request::route('page'),
                                                                                                        'sort' => 'name',
                                                                                                        'direction' => Request::route('direction') == 'asc' ? 'desc' : 'asc']) }}">Название</a> @include('helpers.sort', ['column' => 'name'])</th>
                                    <th><a href="{{ URL::action('Organizations\IndexController@getList', [
                                                                                                        'type' => empty(Request::route('type')) ? 'all' : Request::route('type'),
                                                                                                        'page' => empty(Request::route('page')) ? '1' : Request::route('page'),
                                                                                                        'sort' => 'phone',
                                                                                                        'direction' => Request::route('direction') == 'asc' ? 'desc' : 'asc']) }}">Телефон</a> @include('helpers.sort', ['column' => 'phone'])</th>
                                    <th><a href="{{ URL::action('Organizations\IndexController@getList', [
                                                                                                        'type' => empty(Request::route('type')) ? 'all' : Request::route('type'),
                                                                                                        'page' => empty(Request::route('page')) ? '1' : Request::route('page'),
                                                                                                        'sort' => 'email',
                                                                                                        'direction' => Request::route('direction') == 'asc' ? 'desc' : 'asc']) }}">Email</a> @include('helpers.sort', ['column' => 'email'])</th>
                                    <th>ФИО сотрудника</th>
                                    <th>Email сотрудника</th>
                                    <th>Телефон сотрудника</th>
                                    <th>Просмотр</th>
                                    <th>Удалить</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach ($orgs as $org)
                                    <tr>
                                        <td>{{ $labels[$org->org_type]['long']  }}</td>
                                        <td>{{ $org->name  }}</td>
                                        <td>{{ $org->phone  }}</td>
                                        <td>{{ $org->email  }}</td>
                                        <td>{{ $org->employee_name  }}</td>
                                        <td>{{ $org->employee_email  }}</td>
                                        <td>{{ $org->employee_phone  }}</td>
                                        <td><a href="/org/{{ $org->guid  }}" target="_blank" class="btn btn-block btn-primary">Анкета</a></td>
                                        <td><a href="/org-delete/{{ $org->guid  }}" class="btn btn-block btn-danger delete-action">X</a></td>
                                    </tr>
                                @endforeach

                                </tbody>
                                <!--tfoot>
                                <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                                </tr>
                                </tfoot-->
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <ul class="pagination pagination-sm no-margin pull-right">
                                <li class="paginate_button"><a href="{{ URL::action('Organizations\IndexController@getList', [
                                                                                                        'type' => empty(Request::route('type')) ? 'all' : Request::route('type'),
                                                                                                        'page' => 1,
                                                                                                        'sort' => Request::route('sort'),
                                                                                                        'direction' => Request::route('direction')]) }}">&laquo;</a></li>
                                @php $count = ceil($total / $limit) @endphp
                                @for($i = 1; $i < $count + 1; $i++)
                                    @php $active = ''; if( $i == Request::route('page') ) $active = 'active'; @endphp
                                    <li class="paginate_button {{ $active }}"><a href="{{ URL::action('Organizations\IndexController@getList', [
                                                                                                        'type' => empty(Request::route('type')) ? 'all' : Request::route('type'),
                                                                                                        'page' => $i,
                                                                                                        'sort' => Request::route('sort'),
                                                                                                        'direction' => Request::route('direction')]) }}">{{ $i  }}</a></li>
                                @endfor
                                <li class="paginate_button"><a href="{{ URL::action('Organizations\IndexController@getList', [
                                                                                                        'type' => empty(Request::route('type')) ? 'all' : Request::route('type'),
                                                                                                        'page' => $count,
                                                                                                        'sort' => Request::route('sort'),
                                                                                                        'direction' => Request::route('direction')]) }}">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.box -->

                </div>
            </div>
        </section>
    </div>
    <!-- /.content-wrapper -->

@endsection