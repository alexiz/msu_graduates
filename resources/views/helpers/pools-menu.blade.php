<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <!--li class="header">MAIN NAVIGATION</li-->

            <li class="@if( Route::current()->getName() == 'emailing' ) active @endif">
                <a href="/emailing"><i class="fa fa-envelope-o"></i> <span>Рассылки</span></a>
            </li>
            <li class="@if( Route::current()->getName() == 'pools' ) active @endif">
                <a href="/pools"><i class="fa fa-envelope-o"></i> <span>Пулы адресов</span></a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>