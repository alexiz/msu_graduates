@php
    $direction = Request::route('direction');
    if( empty(Request::route('direction')) )
    {
        $direction = 'asc';
    }
@endphp
@if( Request::route('sort') == $column )
    <i class="fa fa-sort-amount-{{ $direction  }}"></i>
@endif