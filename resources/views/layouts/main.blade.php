<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>МГУ | Выпускники</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="/lte/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/lte/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/lte/dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="/home" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>A</b>LT</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>МГУ</b>Выпускники</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">

            <form class="navbar-form navbar-left" role="search" action="/search" method="GET">
                <div class="form-group">
                    <input name="query" type="text" class="form-control" style="width:400px" id="navbar-search-input" placeholder="Поиск">
                </div>
            </form>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li @if( Route::current()->getName() == 'graduates')) class="active" @endif>
                        <a href="/graduates" class="btn btn-blue btn-flat">Выпускники</a>
                    </li>
                    <li @if( Route::current()->getName() == 'ogranizations')) class="active" @endif>
                        <a href="/orgs" class="btn btn-blue btn-flat">Работодатели</a>
                    </li>
                    <li @if( Route::current()->getName() == 'emailing')) class="active" @endif>
                        <a href="/emailing" class="btn btn-blue btn-flat">Email рассылки</a>
                    </li>
                    <li @if( Route::current()->getName() == 'admin')) class="active" @endif>
                        <a href="/admin/users/list" class="btn btn-blue btn-flat">Администрирование</a>
                    </li>
                    <li>
                        <a href="/auth/logout" class="btn btn-blue btn-flat">Выход</a>
                    </li>
                </ul>

            </div>


        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <!-- Optionally, you can add icons to the links -->
                <li class="active"><a href="/admin/users/list"><i class="fa fa-link"></i> <span>Пользователи</span></a></li>
                <!--li class="active"><a href="/admin/templates/list"><i class="fa fa-link"></i> <span>Шаблоны анкет</span></a></li-->
                <li class="active"><a href="/admin/email-templates/list"><i class="fa fa-link"></i> <span>Email шаблоны</span></a></li>
                <!--li class="treeview">
                    <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="#">Link in level 2</a></li>
                        <li><a href="#">Link in level 2</a></li>
                    </ul>
                </li-->
            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @yield('content')

    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; 2017 <a href="http://www.law.msu.ru/">Law MSU</a>.</strong> All rights
        reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">

    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="/lte/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="/lte/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="/lte/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="/lte/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/lte/dist/js/demo.js"></script>
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script>
    $(function () {
        CKEDITOR.replace('editor1');
    });
</script>
</body>
</html>
