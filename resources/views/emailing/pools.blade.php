@extends('layouts.manager')

@section('content')

    @include('helpers.pools-menu')

    <!-- =============================================== -->


    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Пулы email адресов</h3>
                            <div class="box-tools"><a href="/pools/edit" class="btn btn-block btn-success">Добавить новый</a></div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Название</th>
                                    <th>Редактировать</th>
                                    <th>Список адресов</th>
                                    <th>Добавить адрес</th>
                                    <th>Удалить со всеми адресами</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($dataset as $item)
                                    <tr>
                                        <td>{{ $item->name }}</td>
                                        <td width="100px"><a href="/pools/edit/{{ $item->pool_id }}" class="btn btn-block btn-primary">Редактировать</a></td>
                                        <td width="100px"><a href="/pools/emails/{{ $item->pool_id }}" class="btn btn-block btn-primary">Список</a></td>
                                        <td width="100px"><a href="/pools/emails/add/{{ $item->pool_id }}" class="btn btn-block btn-primary">Добавить адрес</a></td>
                                        <td width="100px"><a href="/pools/delete/{{ $item->pool_id }}" class="btn btn-block btn-danger">Удалить пул</a></td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

@endsection