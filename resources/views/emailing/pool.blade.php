@extends('layouts.manager')

@section('content')


    @include('helpers.pools-menu')

    <!-- =============================================== -->


    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">

                <!-- left column -->
                <div class="col-md-12">
                    @if( count($errors->all()) > 0 )
                        <div class="callout callout-danger">
                            <h4>Ошибки ввода</h4>
                            @foreach ($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                @endif

                <!-- Horizontal Form -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Рассылка</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form class="form-horizontal" method="POST" action="/pools/save{{ isset($item->pool_id) ? '/' . $item->pool_id : '' }}">
                            {!! csrf_field() !!}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputText3" class="col-sm-2 control-label">Название</label>

                                    <div class="col-sm-10">
                                        <input name="pool_name" type="text" class="form-control" id="inputText3" placeholder="Название" value="{{ isset($item->pool_name) ? $item->pool_name : '' }}">
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Сохранить</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                    <!-- /.box -->

                </div>
                <!--/.col (left) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

@endsection