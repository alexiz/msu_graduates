@extends('layouts.manager')

@section('content')

    @include('helpers.pools-menu')

    <!-- =============================================== -->


    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Список адресов в пуле </h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Имя адресата</th>
                                    <th>Email адресата</th>
                                    <th>Дает согласие на обработку перс.данных</th>
                                    <th>Удалить</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($dataset as $item)
                                    @if( !is_null($item->recepient_email) )
                                    <tr>
                                        <td>{{ $item->recepient_name }}</td>
                                        <td>{{ $item->recepient_email }}</td>
                                        <td>@include('helpers.yes-no', ['is_accessable_personal_data' => $item->is_accessable_personal_data])</td>
                                        <td width="100px"><a href="/pools/emails/delete/{{ $item->id }}" class="btn btn-block btn-danger">Удалить адрес</a></td>
                                    </tr>
                                    @endif
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

@endsection