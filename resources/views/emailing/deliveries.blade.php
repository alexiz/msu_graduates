@extends('layouts.manager')

@section('content')

    @include('helpers.pools-menu')

    <!-- =============================================== -->


    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Рассылки</h3>
                            <div class="box-tools"><a href="/emailing/delivery/edit" class="btn btn-block btn-success">Добавить новую</a></div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Тема письма</th>
                                    <th>Шаблон</th>
                                    <th>Вложенный файл</th>
                                    <th>Тестовый email</th>
                                    <th>Тестовое имя получателя</th>
                                    <th>Тестовый запуск</th>
                                    <th>Редактировать</th>
                                    <th>Удалить</th>
                                    <th>Статус</th>
                                    <th>Процесс</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($dataset as $item)
                                    <tr>
                                        <td>{{ $item->subject }}</td>
                                        <td>Шаблон {{ $item->email_template_id }}</td>
                                        <td><a href="/{{ $item->attachment }}">Вложение</a></td>
                                        <td>{{ $item->test_recepient_email }}</td>
                                        <td>{{ $item->test_recepient_name }}</td>
                                        <td width="50px"><a href="/emailing/delivery/test/{{ $item->id }}" class="btn btn-block btn-primary">Тест</a></td>
                                        <td width="70px"><a href="/emailing/delivery/edit/{{ $item->id }}" class="btn btn-block btn-primary">Редактировать</a></td>
                                        <td width="70px"><a href="/emailing/delivery/delete/{{ $item->id }}" class="btn btn-block btn-danger">Удалить</a></td>
                                        <td width="70px">
                                            @if( $queue['delivery_' . $item->id]['active'] == 0 )
                                                <a href="/emailing/delivery/start/{{ $item->id }}" class="btn btn-block btn-danger">Запуск</a>
                                            @endif
                                            @if( $queue['delivery_' . $item->id]['active'] > 0 )
                                                <a href="/emailing/delivery/stop/{{ $item->id }}" class="btn btn-block btn-primary">Стоп</a>
                                            @endif
                                        </td>
                                        <td width="100px">Активных писем в очереди: {{ $queue['delivery_' . $item->id]['active']  }}<br/>
                                            Всего писем в очереди: {{ $queue['delivery_' . $item->id]['all']  }}<br/>
                                            @if( $queue['delivery_' . $item->id]['all'] > 0 )
                                                <a href="/emailing/delivery/clear/{{ $item->id }}" class="btn btn-block btn-danger">Очистить очередь</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

@endsection