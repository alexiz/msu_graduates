@extends('layouts.manager')

@section('content')


    @include('helpers.pools-menu')

    <!-- =============================================== -->


    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">

                <!-- left column -->
                <div class="col-md-12">
                    @if( count($errors->all()) > 0 )
                        <div class="callout callout-danger">
                            <h4>Ошибки ввода</h4>
                            @foreach ($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                @endif

                <!-- Horizontal Form -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Рассылка</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form enctype="multipart/form-data" class="form-horizontal" method="POST" action="/emailing/delivery/save{{ isset($item->id) ? '/' . $item->id : '' }}">
                            {!! csrf_field() !!}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputText3" class="col-sm-2 control-label">Пул email адресов</label>

                                    <div class="col-sm-10">
                                        <select name="pool" class="col-sm-2 form-control">
                                            @foreach ($pools as $pool)
                                                @php $pool_name = $pool['name'];
                                                    if( $pool_name == 'law_world_company')
                                                        $pool_name = '- Международные юридические фирмы';
                                                    if( $pool_name == 'law_russian_company')
                                                        $pool_name = '- Российские юридические фирмы';
                                                    if( $pool_name == 'auditing_company')
                                                        $pool_name = '- Аудиторские компании';
                                                    if( $pool_name == 'com_company')
                                                        $pool_name = '- Коммерческие организации';
                                                    if( $pool_name == 'gov_company')
                                                        $pool_name = '- Государственные организации';
                                                @endphp
                                                <option value="{{ $pool['name'] }}*|*{{ $pool['type'] }}" {{ (isset($item->pool) && $item->pool == $pool['name'] . '*|*' . $pool['type']) ? 'selected' : '' }}>{{ $pool['type2'] }} {{ $pool_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-sm-2 control-label">Тема письма</label>

                                    <div class="col-sm-10">
                                        <input name="subject" type="text" class="form-control" id="inputText3" placeholder="Тема письма" value="{{ isset($item->subject) ? $item->subject : '' }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-sm-2 control-label">Шаблон</label>

                                    <div class="col-sm-10">
                                        <select name="email_template_id" class="col-sm-2 form-control">
                                            @foreach ($templates as $tpl)
                                                <option value="{{ $tpl->id }}" {{ (isset($item->email_template_id) && $item->email_template_id == $tpl->id) ? 'selected' : '' }}>Шаблон {{ $tpl->id }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-sm-2 control-label">Тестовый email</label>

                                    <div class="col-sm-10">
                                        <input name="test_recepient_email" type="text" class="form-control" id="inputText3" placeholder="Тестовый email" value="{{ isset($item->test_recepient_email) ? $item->test_recepient_email : '' }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-sm-2 control-label">Тестовое имя получателя</label>

                                    <div class="col-sm-10">
                                        <input name="test_recepient_name" type="text" class="form-control" id="inputText3" placeholder="Тестовое имя получателя" value="{{ isset($item->test_recepient_name) ? $item->test_recepient_name : '' }}">
                                    </div>
                                </div>
                                <!--div class="form-group">
                                    <label class="col-sm-2 control-label" for="exampleInputFile">Файл</label>
                                    <div class="col-sm-10">
                                        <input type="file" id="exampleInputFile" name="attachment">

                                        <p class="help-block">Приложение к письму</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="exampleInputFile"></label>
                                    <div class="col-sm-10">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remove_attachment">
                                            Удалить вложенный файл
                                        </label>
                                    </div>
                                    </div>
                                </div-->
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Сохранить</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                    <!-- /.box -->

                </div>
                <!--/.col (left) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

@endsection