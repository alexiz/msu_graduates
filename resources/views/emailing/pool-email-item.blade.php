@extends('layouts.manager')

@section('content')


    @include('helpers.pools-menu')

    <!-- =============================================== -->


    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">

                <!-- left column -->
                <div class="col-md-12">
                    @if( count($errors->all()) > 0 )
                        <div class="callout callout-danger">
                            <h4>Ошибки ввода</h4>
                            @foreach ($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                @endif

                <!-- Horizontal Form -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Добавить email в пул "{{ $item->pool_name }}"</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form class="form-horizontal" method="POST" action="/pools/emails/add/{{ $item->pool_id }}">
                            {!! csrf_field() !!}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="recepient_name" class="col-sm-2 control-label">Имя адресата</label>

                                    <div class="col-sm-10">
                                        <input name="recepient_name" type="text" class="form-control" id="recepient_name" placeholder="Имя" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="recepient_email" class="col-sm-2 control-label">Email адресата</label>

                                    <div class="col-sm-10">
                                        <input name="recepient_email" type="text" class="form-control" id="recepient_email" placeholder="Email" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="exampleInputFile"></label>
                                    <div class="col-sm-10">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="is_accessable_personal_data" checked>
                                                Адресат дает согласие на обработку персональных данных
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Сохранить</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                    <!-- /.box -->

                </div>
                <!--/.col (left) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

@endsection