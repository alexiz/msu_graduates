@extends('layouts.main')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Расширенный поиск</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Поисковый запрос ...">
                        </div>
                    </div>

                    <div class="col-md-6">

                        <div class="form-group">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                    Выпускник
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                    Организация
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Программа подготовки выпускников</label>
                            <select class="form-control select2" style="width: 100%;">
                                <option selected="selected">Не выбрано</option>
                                <option>Специалист</option>
                                <option>Бакалавриат</option>
                                <option>Магистратура</option>
                                <option>Второе высшее</option>
                            </select>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>Гражданство выпускников</label>
                            <select class="form-control select2" style="width: 100%;">
                                <option selected="selected">Российская федерация</option>
                                <option>Иное</option>
                            </select>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Работает ли выпускник в настоящее время</label>
                            <select class="form-control select2" style="width: 100%;">
                                <option>Не работает</option>
                                <option>Работает по специальности</option>
                                <option>Работает не по специальности</option>
                            </select>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>Личные данные выпускников</label>
                            <select class="form-control select2" style="width: 100%;">
                                <option selected="selected">Согласен на обработку</option>
                                <option>Не согласен на обработку</option>
                            </select>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>Статус выпускника</label>
                            <select class="form-control select2" style="width: 100%;">
                                <option selected="selected">Находится в поиске работы</option>
                                <option>Не находится в поиске работы</option>
                            </select>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
        </div>
    </section>
    <!-- /.content -->

@endsection