@extends('layouts.manager')


@section('content')
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <section class="sidebar">
            <!--ul class="sidebar-menu">
                    <li>
                        <a href="/search/custom">Расширенный поиск</a>
                    </li>
            </ul-->
        </section>
    </aside>

    <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Поисковый запрос: {{ $query  }}</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <table class="table">
                            @foreach($results as $item)
                                <tr>
                                    <td><a href="{{ $item['url'] }}" target="_blank">{{ $item['name'] }}</a></td>
                                    <td>{{ $item['email'] }}</td>
                                    <td>{{ $item['phone'] }}</td>
                                    <td>{{ $item['fld'] }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>


            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    </div>

@endsection