﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 7.2.53.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 04.06.2017 21:11:46
-- Версия сервера: 5.5.5-10.0.17-MariaDB
-- Версия клиента: 4.1
--


-- 
-- Отключение внешних ключей
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Установить режим SQL (SQL mode)
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

--
-- Описание для таблицы deliveries
--
DROP TABLE IF EXISTS deliveries;
CREATE TABLE deliveries (
  id INT(11) NOT NULL AUTO_INCREMENT,
  subject VARCHAR(255) DEFAULT NULL,
  email_template_id VARCHAR(255) DEFAULT NULL,
  test_recepient_email VARCHAR(255) DEFAULT NULL,
  test_recepient_name VARCHAR(255) DEFAULT NULL,
  attachment VARCHAR(255) DEFAULT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  pool VARBINARY(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 4
AVG_ROW_LENGTH = 16384
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы delivery_queues
--
DROP TABLE IF EXISTS delivery_queues;
CREATE TABLE delivery_queues (
  job_id INT(11) DEFAULT NULL,
  delivery_id INT(11) DEFAULT NULL,
  pause TINYINT(1) DEFAULT NULL,
  is_busy TINYINT(1) DEFAULT NULL,
  recepient_email VARCHAR(255) DEFAULT NULL,
  recepient_name VARCHAR(255) DEFAULT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  INDEX IDX_delivery_queues_delivery_i (delivery_id),
  INDEX IDX_delivery_queues_job_id (job_id)
)
ENGINE = INNODB
AVG_ROW_LENGTH = 16384
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы email_pools
--
DROP TABLE IF EXISTS email_pools;
CREATE TABLE email_pools (
  id INT(11) NOT NULL AUTO_INCREMENT,
  pool_name VARCHAR(255) DEFAULT NULL,
  recepient_email VARCHAR(255) DEFAULT NULL,
  recepient_name VARCHAR(255) DEFAULT NULL,
  is_accessable_personal_data TINYINT(1) DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  created_at DATETIME DEFAULT NULL,
  pool_id INT(11) DEFAULT NULL,
  PRIMARY KEY (id),
  INDEX UK_email_pools_pool_name (pool_name)
)
ENGINE = INNODB
AUTO_INCREMENT = 12
AVG_ROW_LENGTH = 5461
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы email_templates
--
DROP TABLE IF EXISTS email_templates;
CREATE TABLE email_templates (
  id INT(11) NOT NULL AUTO_INCREMENT,
  body TEXT DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  created_at DATETIME DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 5
AVG_ROW_LENGTH = 16384
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы employees
--
DROP TABLE IF EXISTS employees;
CREATE TABLE employees (
  id INT(11) NOT NULL AUTO_INCREMENT,
  organization_id INT(11) DEFAULT NULL,
  name VARCHAR(500) DEFAULT NULL,
  phone INT(11) DEFAULT NULL,
  email VARCHAR(50) DEFAULT NULL,
  `position` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы form_templates
--
DROP TABLE IF EXISTS form_templates;
CREATE TABLE form_templates (
  id INT(11) NOT NULL AUTO_INCREMENT,
  template TEXT DEFAULT NULL,
  type VARCHAR(50) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX UK_form_templates_type (type)
)
ENGINE = INNODB
AUTO_INCREMENT = 5
AVG_ROW_LENGTH = 5461
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы graduates
--
DROP TABLE IF EXISTS graduates;
CREATE TABLE graduates (
  id INT(11) NOT NULL AUTO_INCREMENT,
  form_id INT(11) DEFAULT NULL,
  graduation_year INT(11) DEFAULT NULL,
  student_id INT(11) DEFAULT NULL,
  name VARCHAR(500) DEFAULT NULL,
  email VARCHAR(255) DEFAULT NULL,
  phone VARCHAR(20) DEFAULT NULL,
  group_number VARBINARY(20) DEFAULT NULL,
  program VARCHAR(50) DEFAULT NULL,
  current_employer_name VARCHAR(500) DEFAULT NULL,
  citizenship VARCHAR(50) DEFAULT NULL,
  is_accessable_personal_data TINYINT(1) DEFAULT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  form_template_id INT(11) DEFAULT NULL,
  guid VARCHAR(32) DEFAULT NULL,
  filepath VARCHAR(255) DEFAULT NULL,
  skills TEXT DEFAULT NULL,
  is_looking_for_work INT(11) DEFAULT NULL,
  PRIMARY KEY (id),
  INDEX IDX_graduates_guid (guid)
)
ENGINE = INNODB
AUTO_INCREMENT = 21
AVG_ROW_LENGTH = 2340
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы organizations
--
DROP TABLE IF EXISTS organizations;
CREATE TABLE organizations (
  id INT(11) NOT NULL AUTO_INCREMENT,
  form_id INT(11) DEFAULT NULL,
  org_type VARCHAR(50) DEFAULT NULL,
  name VARCHAR(500) DEFAULT NULL,
  phone VARCHAR(20) DEFAULT NULL,
  address VARCHAR(500) DEFAULT NULL,
  email VARCHAR(50) DEFAULT NULL,
  employee_name VARCHAR(255) DEFAULT NULL,
  employee_phone VARCHAR(255) DEFAULT NULL,
  employee_email VARCHAR(255) DEFAULT NULL,
  employee_position VARCHAR(255) DEFAULT NULL,
  comment VARCHAR(500) DEFAULT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  form_template_id INT(11) DEFAULT NULL,
  guid VARCHAR(32) DEFAULT NULL,
  filepath VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 5
AVG_ROW_LENGTH = 4096
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы roles
--
DROP TABLE IF EXISTS roles;
CREATE TABLE roles (
  id INT(11) NOT NULL AUTO_INCREMENT,
  role VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 5
AVG_ROW_LENGTH = 4096
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы users
--
DROP TABLE IF EXISTS users;
CREATE TABLE users (
  id INT(11) NOT NULL AUTO_INCREMENT,
  email VARCHAR(50) DEFAULT NULL,
  password VARCHAR(60) NOT NULL,
  role_id INT(11) NOT NULL DEFAULT 1,
  working_till DATETIME DEFAULT NULL,
  is_blocked TINYINT(1) NOT NULL DEFAULT 1,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  remember_token VARCHAR(100) DEFAULT NULL,
  name VARCHAR(255) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX UK_users_email (email)
)
ENGINE = INNODB
AUTO_INCREMENT = 2
AVG_ROW_LENGTH = 16384
CHARACTER SET utf8
COLLATE utf8_general_ci;

-- 
-- Вывод данных для таблицы deliveries
--
INSERT INTO deliveries VALUES
(3, 'Приветственное письмо', '4', 'izumsky@gmail.com', 'Александр Ворошин', '', '2017-05-21 17:44:03', '2017-05-21 19:16:23', 'law_russian_company*|*org');

-- 
-- Вывод данных для таблицы delivery_queues
--
INSERT INTO delivery_queues VALUES
(1, 3, 1, 0, 'izumsky@gmail.com', 'Александр Ворошин', '2017-05-21 19:19:48', '2017-05-21 19:21:59');

-- 
-- Вывод данных для таблицы email_pools
--
INSERT INTO email_pools VALUES
(8, 'Новый год', NULL, NULL, NULL, '2017-05-21 09:44:03', '2017-05-21 09:30:32', 1),
(10, 'Новый год', 'snegurka@yandex.ru', 'Снегурочка', 1, '2017-05-21 09:44:03', '2017-05-21 09:31:06', 1),
(11, 'Новый год', 'snowman@gmail.com', 'Снеговик', 1, '2017-05-21 09:44:03', '2017-05-21 09:31:21', 1);

-- 
-- Вывод данных для таблицы email_templates
--
INSERT INTO email_templates VALUES
(4, '<p>Здравствуйте, {FULL_NAME}</p>\r\n\r\n<p>Вас приветсвует портал резюме МГУ.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><em>С уважением,</em></p>\r\n\r\n<p><em>МГУ</em></p>', '2017-05-21 19:13:41', '2017-05-21 19:13:41');

-- 
-- Вывод данных для таблицы employees
--

-- Таблица msu_graduates.employees не содержит данных

-- 
-- Вывод данных для таблицы form_templates
--
INSERT INTO form_templates VALUES
(1, 'resources/templates/form.xml', 'main'),
(3, 'resources/templates/cv.xml', 'cv'),
(4, 'resources/templates/org.xml', 'org');

-- 
-- Вывод данных для таблицы graduates
--
INSERT INTO graduates VALUES
(14, NULL, 2014, 1, 'Пушкин Александр Сергеевич', 'pushkin@gmail.com', '+79096272217', x'12', 'specialist', NULL, 'rf', 1, '2017-03-16 14:19:41', '2017-04-02 14:27:55', '2017-04-02 14:27:55', 1, 'ad91d76b6f0c5e67e3209ebfed53e8c3', 'resources/templates/forms/ad91d76b6f0c5e67e3209ebfed53e8c3.xml', NULL, NULL),
(15, NULL, 2014, 34, 'Достоевский Федор Михайлович', 'dostoevsky@mail.ru', '+79106182117', NULL, 'specialist', NULL, 'rf', 1, '2017-03-16 14:20:25', '2017-03-16 14:20:25', NULL, 1, '6e04a1962c6f2414260951f57a6605f4', 'resources/templates/forms/6e04a1962c6f2414260951f57a6605f4.xml', NULL, NULL),
(16, NULL, 2014, 451, 'Ломоносов Михаил Васильевич', 'lomonosov@yahoo.com', '+79096272217', NULL, 'bachelor', NULL, 'rf', 1, '2017-03-16 14:21:30', '2017-04-02 14:29:08', '2017-04-02 14:29:08', 1, '0c2f54446b6f4e7d7ab33bb6512e7cc4', 'resources/templates/forms/0c2f54446b6f4e7d7ab33bb6512e7cc4-removed.xml', NULL, NULL),
(17, NULL, 2014, 21, 'Гоголь Николай Васильевич', 'gogol@mail.ru', '+79096272217', NULL, 'master', NULL, 'rf', 1, '2017-03-16 14:22:37', '2017-03-16 14:22:37', NULL, 1, 'bcac0fcef98b0b5cd9f95246ce7f87be', 'resources/templates/forms/bcac0fcef98b0b5cd9f95246ce7f87be.xml', NULL, NULL),
(18, NULL, 2014, 45, 'Умберто Эко', 'eco@eco.ru', '+12395894534', NULL, 'master', NULL, 'rf', 1, '2017-03-16 14:23:33', '2017-03-16 14:23:33', NULL, 1, 'e1effdaaf9d95ee8c8a11df24e1e0964', 'resources/templates/forms/e1effdaaf9d95ee8c8a11df24e1e0964.xml', NULL, NULL),
(19, NULL, 2014, 22, 'Томас Манн', 'mann@post.ru', '+12395894534', NULL, 'second', NULL, 'rf', 1, '2017-03-16 14:24:29', '2017-03-16 14:24:29', NULL, 1, '7a3c850a1b7d25fe038b391fba6f93fc', 'resources/templates/forms/7a3c850a1b7d25fe038b391fba6f93fc.xml', NULL, NULL),
(20, NULL, 2015, 23, 'Лермонтов Михаил Юрьевич', 'lermontov@mail.ru', '+12395894534', NULL, 'master', NULL, 'rf', 1, '2017-03-16 14:25:39', '2017-06-04 16:12:27', NULL, 1, '07773d1924f6ec7d8edcbc43895ab8b8', 'resources/templates/forms/07773d1924f6ec7d8edcbc43895ab8b8.xml', NULL, NULL);

-- 
-- Вывод данных для таблицы organizations
--
INSERT INTO organizations VALUES
(1, NULL, 'law_russian_company', 'YAGA_Tracking', '+79096272217', 'Профсоюзная', 'alexmegus@yandex.ru', 'sdsdf sdfsdf sdf s', 'sdfsd', 'zxxxxf@smail.ru', 'sdfsdf', NULL, '2017-03-29 20:39:50', '2017-03-29 20:39:50', NULL, NULL, NULL, NULL),
(2, NULL, 'gov_company', 'HotRabbit', '+79106182117', 'Кржижановского', 'test@test.com', 'Бауман Николай', 'sdfsd', 'revolution@mail.ru', 'революционер', NULL, '2017-03-29 20:42:47', '2017-03-29 20:42:47', NULL, NULL, NULL, NULL),
(3, NULL, 'com_company', 'Mark-SQL', '+79106182117', 'Бауманская', 'dallapikkola@gmail.com', 'Бауман Николай', 'sdfsd', 'revolution@mail.ru', 'революционер', 'ываыва', '2017-03-29 20:45:11', '2017-04-02 14:33:45', '2017-04-02 14:33:45', 4, '157b77ebdc1d4d83326d736562e30313', 'resources/templates/orgs/157b77ebdc1d4d83326d736562e30313-removed.xml'),
(4, NULL, 'law_world_company', 'Международная юридическая фирма 123', '8 909 627-22-17', 'Международная юридическая фирма 123 Международная юридическая фирма 123', 'kshenina@gmail.com', 'Ричи Блэкмор', '+1234567', 'deeppurple@mail.com', 'директор', 'sddsf', '2017-04-02 14:36:15', '2017-04-02 14:37:45', NULL, 4, '46f40a1d32dc0f6a3cd4f26ef0cd569a', 'resources/templates/orgs/46f40a1d32dc0f6a3cd4f26ef0cd569a.xml');

-- 
-- Вывод данных для таблицы roles
--
INSERT INTO roles VALUES
(1, 'Студент'),
(2, 'Пользователь'),
(3, 'Менеджер'),
(4, 'Администратор');

-- 
-- Вывод данных для таблицы users
--
INSERT INTO users VALUES
(1, 'test@test.com', '$2y$10$HbMSKQ2Art4CIJ/7rvkuO.umUZx4UxuRpH8atc4DgRXA0CEI8OupG', 4, NULL, 1, '2017-05-14 19:36:36', '2017-05-14 19:36:36', NULL, 'Alexander');

-- 
-- Восстановить предыдущий режим SQL (SQL mode)
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Включение внешних ключей
-- 
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;