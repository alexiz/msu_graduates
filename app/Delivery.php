<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subject', 'email_template_id', 'test_recepient_email', 'test_recepient_name', 'attachment', 'pool'
    ];
}
