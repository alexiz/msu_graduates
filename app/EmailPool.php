<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class EmailPool extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pool_name', 'recepient_email', 'recepient_name', 'is_accessable_personal_data', 'pool_id'
    ];

    public function scopeGetMaxPoolId()
    {
        return $this->select('*')
            ->orderBy('pool_id', 'desc')
            ->first();
    }

    public function scopeGetEmailPools()
    {
        return $this->select(DB::raw('pool_name as name'), DB::raw('COUNT(pool_name) as total'), DB::raw("'pool' as type"), DB::raw("'Email пул' as type2"), 'pool_id')
            ->groupBy('pool_name', 'pool_id')
            ->orderBy('pool_name');
    }

    public function scopeGetEmails($query, $pool_type, $pool_name, $is_accessable_personal_data = false)
    {
        if( $pool_type != 'pool' )
        {
            return $this->select(DB::raw('recepient_email as email'), DB::raw('recepient_name as name'))->where('id', 0);
        }
        $q = $this->select(DB::raw('recepient_email as email'), DB::raw('recepient_name as name'))
            ->whereNotNull('recepient_email')
            ->where('pool_name', $pool_name);

        if($is_accessable_personal_data)
        {
            $q->where('is_accessable_personal_data', $is_accessable_personal_data);
        }
        return $q;
    }
}
