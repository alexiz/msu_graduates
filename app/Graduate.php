<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Graduate extends Model
{
    protected $programs = ['specialist' => 'Специалист',
                            'bachelor' => 'Бакалавриат',
                            'master' => 'Магистратура',
                            'second' => 'Второе высшее'];

    protected $fillable = ['graduation_year', 'student_id', 'name', 'email', 'phone',
                            'group_number', 'program', 'citizenship', 'current_employer_name',
                            'is_accessable_personal_data',
                            'form_template_id', 'guid', 'filepath', 'updated_at'];

    public function scopeSpecializationTree($query)
    {
        return $this->select('graduation_year', 'program', DB::raw('COUNT(program) as total'))
            ->whereNull('deleted_at')
            ->groupBy('graduation_year', 'program')
            ->orderBy('graduation_year', 'desc');
    }

    protected function getPreparedParams($fields, $page = 1, $limit = 50, $sort = null, $direction = 'asc')
    {
        if( (int)$page < 1 || (int)$page > 9999999 )
        {
            $page = 1;
        }

        if( is_null($sort) || !in_array($sort, $fields) )
        {
            $sort = $fields[0];
        }

        if( !in_array($direction, ['asc', 'desc']) )
        {
            $direction = 'asc';
        }

        return ['offset' => ($page - 1) * $limit, 'limit' => $limit, 'sort' => $sort, 'direction' => $direction];
    }

    public function scopeGetList($query, $year, $program, $page, $limit, $sort, $direction)
    {
        $fields = ['id','graduation_year',
                    'student_id',
                    'name',
                    'email',
                    'phone',
                    'citizenship',
                    'is_looking_for_work',
                    'guid',
                    'comment', 'updated_at'];
        if( is_null($year) || (int)$year < 1900 || (int)$year > 2500 )
        {
            $year = date("Y");
        }

        $params = $this->getPreparedParams($fields, $page, $limit, $sort, $direction);


        $q = $this->select($fields)
            ->whereNull('deleted_at')
            ->where('graduation_year', $year)
            ->orderBy($params['sort'], $params['direction'])
            ->offset($params['offset'])
            ->limit($params['limit']);

        if( isset($this->programs[$program]))
        {
            $q->where('program', $program);
        }
        //var_dump($q->toSql());exit();

        return $q;
    }

    public function scopeGetEmailPools()
    {
        return $this->select(DB::raw('graduation_year as name'), DB::raw('COUNT(graduation_year) as total'), DB::raw("'graduate' as type"), DB::raw("'Выпускники' as type2"))
            ->whereNull('deleted_at')
            ->groupBy('graduation_year')
            ->orderBy('graduation_year', 'desc');
    }

    public function scopeGetEmails($query, $type, $year, $is_accessable_personal_data = false)
    {
        if( $type != 'graduate' )
        {
            return $this->select('email', 'name', DB::raw('concat(\'http://careerlawmsu.ru/questionaire/student/\', guid) as form_link'))->where('id', 0);
        }
        $q = $this->select('email', 'name', DB::raw('concat(\'http://careerlawmsu.ru/questionaire/student/\', guid) as form_link'))
            ->whereNull('deleted_at')
            ->where('graduation_year', $year);

        if($is_accessable_personal_data)
        {
            $q->where('is_accessable_personal_data', $is_accessable_personal_data);
        }
        return $q;
    }
}
