<?php

namespace App\Http\Controllers\Emailing;

use App\Delivery;
use App\EmailTemplate;

use App\Graduate;
use App\Organization;
use App\EmailPool;
use App\DeliveryQueue;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeliveryController extends Controller
{

    public function getList()
    {
        $dataset = Delivery::all();
        $queue = [];
        foreach ($dataset as $row)
        {
            $all = DeliveryQueue::where("delivery_id", $row->id)->count();
            $active = DeliveryQueue::where("delivery_id", $row->id)->where("pause", 0)->count();
            $queue['delivery_' . $row->id] = ['all' => $all, 'active' => $active];
        }
        return view('emailing.deliveries', ['dataset' => $dataset, 'queue' => $queue]);
    }

    public function removeOne(Request $request, $id)
    {
        $item = Delivery::find($id);
        if(isset($item->id))
        {
            $item->delete();
        }
        return redirect('emailing');
    }

    public function editOne(Request $request, $id = null)
    {
        $item = Delivery::find($id);
        $templates = EmailTemplate::all();


        $graduate_pool = Graduate::getEmailPools()->get()->toArray();
        $org_pool = Organization::getEmailPools()->get()->toArray();
        $email_pool = EmailPool::getEmailPools()->get()->toArray();

        $pools = array_merge($graduate_pool, $org_pool, $email_pool);

        return view('emailing.delivery', ['item' => $item, 'templates' => $templates, 'pools' => $pools]);
    }

    public function saveOne(Request $request, $id = null)
    {
        $row = ['subject' => $request->subject,
                'email_template_id' => $request->email_template_id,
                'test_recepient_email' => $request->test_recepient_email,
                'test_recepient_name' => $request->test_recepient_name,
                'pool' => $request->pool];
        $filename = '';
        if( !is_null($request->attachment) )
        {
            $filename = $request->file('attachment')->store('disks', 'public');
        }
        $row['attachment'] = $filename;
        if( !empty($row['attachment']) && isset($request->remove_attachment) && $request->remove_attachment )
        {
            $row['attachment'] = '';
        }
        if( is_null($id) )
        {
            Delivery::create($row);
        }
        else
        {
            Delivery::where('id', $id)->update($row);
        }
        return redirect('emailing');
    }

    public function start(Request $request, $id)
    {
        $queue = DeliveryQueue::where("delivery_id", $id)->count();
        if( $queue > 0 )
        {
            DeliveryQueue::where('delivery_id', $id)->update(['pause' => 0]);
            return redirect('emailing');
        }
        $item = Delivery::find($id);
        $poolInfo = explode('*|*', $item->pool);

        $models = [new Graduate(), new Organization(), new EmailPool()];
        $emails = [];
        for($i = 0; $i < count($models); $i++)
        {
            $poolType = $poolInfo[1];
            $poolName = $poolInfo[0];
            $emails = $models[$i]->getEmails($poolType, $poolName)->get()->toArray();
            if( count($emails) > 0 )
            {
                break;
            }
        }

        $job = DeliveryQueue::getMaxJobId()->first();
        if( isset($job->job_id) ) {
            $newJobId = (int)$job->job_id + 1;
        } else {
            $newJobId = 1;
        }
        $row = ['job_id' => $newJobId, 'delivery_id' => $id];
        foreach ($emails as $item)
        {
            $row['pause'] = 1;
            $row['is_busy'] = 0;
            $row['recepient_email'] = $item['email'];
            $row['recepient_name'] = $item['name'];
            if( isset($item['form_link']) ) {
                $row['form_link'] = $item['form_link'];
            }
            DeliveryQueue::create($row);
        }
        DeliveryQueue::where('job_id', $newJobId)->update(['pause' => 0]);

        return redirect('emailing');
    }

    public function stop(Request $request, $id)
    {
        DeliveryQueue::where('delivery_id', $id)->update(['pause' => 1]);
        return redirect('emailing');
    }

    public function clear(Request $request, $id)
    {
        DeliveryQueue::where('delivery_id', $id)->delete();
        return redirect('emailing');
    }

    public function test(Request $request, $id)
    {
        $queue = DeliveryQueue::where("delivery_id", $id)->count();
        if( $queue > 0 )
        {
            DeliveryQueue::where('delivery_id', $id)->update(['pause' => 0]);
            return redirect('emailing');
        }
        $item = Delivery::find($id);

        $job = DeliveryQueue::getMaxJobId()->get();
        if( isset($job->job_id) ) {
            $newJobId = (int)$job->job_id + 1;
        } else {
            $newJobId = 1;
        }
        $row = ['job_id' => $newJobId, 'delivery_id' => $id];
        $row['pause'] = 0;
        $row['is_busy'] = 0;
        $row['recepient_email'] = $item->test_recepient_email;
        $row['recepient_name'] = $item->test_recepient_name;
        DeliveryQueue::create($row);

        return redirect('emailing');
    }
}
