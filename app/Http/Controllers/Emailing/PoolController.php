<?php

namespace App\Http\Controllers\Emailing;

use App\Delivery;
use App\EmailTemplate;
use App\EmailPool;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PoolController extends Controller
{
    private $validator  = ['recepient_email' => "required|email|max:250"];

    public function getList()
    {
        $dataset = EmailPool::getEmailPools()->get();
        return view('emailing.pools', ['dataset' => $dataset]);
    }

    public function removeOne(Request $request, $id)
    {
        EmailPool::where('pool_id', $id)->delete();
        return redirect('pools');
    }

    public function editOne(Request $request, $pool_id = null)
    {
        $item = EmailPool::where('pool_id', $pool_id)->first();

        return view('emailing.pool', ['item' => $item]);
    }

    public function saveOne(Request $request, $pool_id = null)
    {
        $row = ['pool_name' => $request->pool_name];
        if( is_null($pool_id) )
        {
            $poolItem = EmailPool::getMaxPoolId()->first();
            if( isset($poolItem->pool_id) ) {
                $newPoolId = (int)$poolItem->pool_id + 1;
            } else {
                $newPoolId = 1;
            }
            $row['pool_id'] = $newPoolId;
            EmailPool::create($row);
        }
        else
        {
            EmailPool::where('pool_id', $pool_id)->update($row);
        }
        return redirect('pools');
    }

    public function emailToPoolForm(Request $request, $pool_id)
    {
        $item = EmailPool::where('pool_id', $pool_id)->first();
        return view('emailing.pool-email-item', ['item' => $item]);
    }

    public function addEmailToPool(Request $request, $pool_id)
    {
        $validator = $this->getValidationFactory()->make($request->all(), $this->validator);
        if ($validator->fails())
        {
            return redirect()->to('/pools/emails/add/' . $pool_id)
                ->withInput($request->input())
                ->withErrors($validator->errors()->getMessages(), $this->errorBag());
        }
        else
        {
            $item = EmailPool::where('pool_id', $pool_id)->first();
            if( isset($request->is_accessable_personal_data) && !empty($request->is_accessable_personal_data) )
            {
                $request->is_accessable_personal_data = 1;
            }
            $row = ['pool_id' => $pool_id,
                'pool_name' => $item->pool_name,
                'recepient_email' => $request->recepient_email,
                'recepient_name' => $request->recepient_name,
                'is_accessable_personal_data' => $request->is_accessable_personal_data];
            EmailPool::create($row);
        }

        return redirect('pools');
    }

    public function getListOfEmails(Request $request, $pool_id)
    {
        $dataset = EmailPool::where('pool_id', $pool_id)->get();
        return view('emailing.pool-email-list', ['dataset' => $dataset]);
    }

    public function removeEmailFromPool(Request $request, $id)
    {
        EmailPool::where('id', $id)->delete();
        return redirect('pools');
    }
}
