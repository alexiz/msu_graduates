<?php

namespace App\Http\Controllers\Graduates;

use App\Graduate;
use App\Http\Traits;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    use Traits\RoleTrait;
    private function getRussianLabelForProgram($program)
    {
        $labels = ['specialist' => 'Специалист',
                    'bachelor' => 'Бакалавриат',
                    'master' => 'Магистратура',
                    'second' => 'Второе высшее'];
        if( !isset($labels[$program]) )
        {
            return null;
        }
        return $labels[$program];
    }

    public function getList($year = null, $program = null, $page = 1, $sort = 'name', $direction = 'asc')
    {
        $tree = Graduate::specializationTree()->get();
        $treeAssoc = [];
        foreach ($tree as $item)
        {
            $treeAssoc[$item->graduation_year][] = ['program' => $item->program,
                                                    'label' => $this->getRussianLabelForProgram($item->program),
                                                    'total' => $item->total];
        }

        $limit = 50;
        $graduates = Graduate::getList($year, $program, $page, $limit, $sort, $direction)->get();
        $user = Auth::user();
        $readonly = '';
        if( $this->isUserRole($user->role_id) )
        {
            $readonly = '-readonly';
        }

        return view('graduates.graduates' . $readonly, ['tree' => $treeAssoc,
                                            'graduates' => $graduates,
                                            'year_title' => empty($year) ? date("Y") : $year,
                                            'program_title' => $this->getRussianLabelForProgram($program),
                                            'limit' => $limit]);
    }

    public function deleteItem($guid)
    {
        $org = Graduate::where('guid', $guid)->first();
        if( isset($org->filepath) ) {
            $newFilename = str_replace('.xml', '-removed.xml', $org->filepath);
            rename(base_path($org->filepath), base_path($newFilename));
            $date = new \DateTime();
            Graduate::where('guid', $org->guid)->update(['deleted_at' => $date->format('Y-m-d H:i:s'), 'filepath' => $newFilename]);
        }
        return redirect(request()->headers->get('referer'));
    }
    
    public function commentGet($id)
    {
        $tree = Graduate::specializationTree()->get();
        $treeAssoc = [];
        foreach ($tree as $item)
        {
            $treeAssoc[$item->graduation_year][] = ['program' => $item->program,
                'label' => $this->getRussianLabelForProgram($item->program),
                'total' => $item->total];
        }

        $graduate = Graduate::where('id', $id)->first();
        return view('graduates.comment', ['tree' => $treeAssoc, 'graduate' => $graduate]);
    }

    public function commentEdit(Request $request, $id)
    {
        $row = ['comment' => $request->comment];
        Graduate::where('id', $id)->update($row);
        return redirect('graduates/comment/' . $id);
    }
}
