<?php

namespace App\Http\Controllers\Forms;

use App\FormTemplate;
use App\Graduate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

const TEMPLATES = 'resources/templates/';

class CvController extends Controller
{
    public function getStudentFormReadonly($id)
    {
        $graduates = Graduate::find((int)$id);
        if( !isset($graduates->id) )
        {
            abort(404, 'Page not found');
        }
        
        $xmlPath = base_path(TEMPLATES . 'cv/' . $graduates->guid . '.xml');
        $xslPath = base_path(TEMPLATES . 'form-disabled.xsl');

        if( !file_exists($xmlPath) )
        {
            abort(404, 'Page not found');
        }

        $xml = new \DOMDocument;
        $xml->load($xmlPath);
        $xsl = new \DOMDocument;
        $xsl->load($xslPath);
        $proc = new \XSLTProcessor;
        $proc->importStyleSheet($xsl); // attach the xsl rules
        $res = $proc->transformToXML($xml);

        return view('forms.cv-readonly', ['active' => 'cv', 'form' => $res, 'id' => (int)$id]);
    }

    public function getStudentForm($guid)
    {
        $xmlPath = base_path(TEMPLATES . 'cv/' . $guid . '.xml');
        $xslPath = base_path(TEMPLATES . 'form-disabled.xsl');

        if( !file_exists($xmlPath) )
        {
            return redirect('questionaire/student/');
        }

        $xml = new \DOMDocument;
        $xml->load($xmlPath);

        $xsl = new \DOMDocument;
        $xsl->load($xslPath);

        $proc = new \XSLTProcessor;
        $proc->importStyleSheet($xsl); // attach the xsl rules

        $res = $proc->transformToXML($xml);

        return view('forms.cv', ['active' => 'cv', 'form' => $res, 'guid' => !empty($guid) ? $guid : 'new']);
    }

    public function saveStudentForm(Request $request, $guid)
    {
        $xmlPath = base_path(TEMPLATES . 'cv/' . $guid . '.xml');

        $validator = $this->getValidationFactory()->make($request->all(), ['is_looking_for_work' => 'required|integer']);
        if ($validator->fails())
        {
            $this->saveXml($request, $xmlPath, $guid);
            return redirect()->to('cv/' . $guid)
                ->withInput($request->input())
                ->withErrors($validator->errors()->getMessages(), $this->errorBag());
        }

        $graduate = ['skills' => $request->skills,
                    'is_looking_for_work' => $request->is_looking_for_work];


        $collection = Graduate::where('guid', $guid);
        if( $collection->count() > 0 )
        {
            $collection->update($graduate);
            $xmlPath = base_path(TEMPLATES . 'cv/' . $guid . '.xml');
            $this->saveXml($request, $xmlPath, $guid);
        }

        return redirect('cv/' . $guid);
    }

    protected function saveXml(Request $request, $xmlPath, $guid)
    {
        $xml = simplexml_load_file($xmlPath);
        foreach ($request->all() as $key => $value) {
            $type = 'input';
            $obj = $xml->xpath('//input[@field="' . $key . '"]');
            if( (!isset($obj) || empty($obj)) && !is_array($value) )
            {
                $type = 'textarea';
                $obj = $xml->xpath('//textarea[@field="' . $key . '"]');
            }
            if( (!isset($obj) || empty($obj)) && !is_array($value) )
            {
                $type = 'radio';
                $radios = $xml->xpath('//radio[@field="' . $key . '"]');
                for($i = 0; $i < count($radios); $i++)
                {
                    $radios[$i]['checked'] = 'false';
                }
                $obj = $xml->xpath('//radio[@field="' . $key . '"][@value="' . $value . '"]');
            }
            if( !isset($obj) || empty($obj) )
            {
                $type = 'checkbox';
                $checkboxes = $xml->xpath('//checkbox[@field="' . $key . '[]"]');
                for($i = 0; $i < count($checkboxes); $i++)
                {
                    $checkboxes[$i]['checked'] = 'false';
                }
                for($i = 0; $i < count($value); $i++)
                {
                    $obj = $xml->xpath('//checkbox[@field="' . $key . '[]"][@value="' . $value[$i] . '"]');
                    if( isset($obj) && !empty($obj) ) {
                        $obj[0]['checked'] = 'true';
                    }
                }
            }

            if( isset($obj) && !empty($obj) ) {
                if ($type == 'input' || $type == 'textarea') {
                    $obj[0][0] = $value;
                } elseif($type == 'radio') {
                    $obj[0]['checked'] = 'true';
                }
            }
        }
        file_put_contents($xmlPath, $xml->asXml());

        return $xmlPath;
    }
}
