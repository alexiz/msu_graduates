<?php

namespace App\Http\Controllers\Forms;

use App\FormTemplate;
use App\Graduate;
use App\Organization;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

const TEMPLATES = 'resources/templates/';

class DocxController extends Controller
{
    public function downloadStudentForm($id)
    {
        $graduates = Graduate::find((int)$id);
        if( !isset($graduates->id) )
        {
            abort(404, 'Page not found');
        }


        $xmlPath = base_path(TEMPLATES . 'forms/' . $graduates->guid . '.xml');
        $xslPath = base_path(TEMPLATES . 'docx.xsl');

        $sourceTemplate = base_path(TEMPLATES . 'template.docx');
        $outputDocument = base_path(TEMPLATES . 'forms/' . $graduates->guid . '.docx');

        if( !file_exists($xmlPath) )
        {
            abort(404, 'Page not found');
        }

        $xml = new \DOMDocument;
        $xml->load($xmlPath);
        $xsl = new \DOMDocument;
        $xsl->load($xslPath);
        $proc = new \XSLTProcessor;
        $proc->importStyleSheet($xsl);
        $res = $proc->transformToXML($xml);

        if (copy($sourceTemplate, $outputDocument)) {
            $zipArchive = new \ZipArchive();
            $zipArchive->open($outputDocument);
            $zipArchive->addFromString("word/document.xml", $res);
            $zipArchive->close();

            $size   = filesize($outputDocument);

            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . $graduates->guid . '.docx');
            header('Content-Transfer-Encoding: binary');
            header('Connection: Keep-Alive');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . $size);
            readfile($outputDocument);
        } else {
            redirect('student/form/' . $id);
        }
    }

}
