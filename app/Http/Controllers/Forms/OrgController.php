<?php

namespace App\Http\Controllers\Forms;

use App\FormTemplate;
use App\Organization;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Http\Traits;

const TEMPLATES = 'resources/templates/';

class OrgController extends Controller
{
    use Traits\RoleTrait;
    public function getOrgFormReadonly($id)
    {
        $org = Organization::find((int)$id);
        if( !isset($org->id) )
        {
            abort(404, 'Page not found');
        }

        $xmlPath = base_path(TEMPLATES . 'orgs/' . $org->guid . '.xml');
        $xslPath = base_path(TEMPLATES . 'form.xsl');


        if( !file_exists($xmlPath) )
        {
            abort(404, 'Page not found');
        }

        $xml = new \DOMDocument;
        $xml->load($xmlPath);
        $xsl = new \DOMDocument;
        $xsl->load($xslPath);
        $proc = new \XSLTProcessor;
        $proc->importStyleSheet($xsl); // attach the xsl rules
        $res = $proc->transformToXML($xml);

        return view('forms.org-readonly', ['form' => $res, 'id' => (int)$id]);
    }

    public function getOrgForm($guid = null)
    {
        $existedOrgGuid = $this->redirectToExistedOrgIfUserRole($guid);
        if( $existedOrgGuid )
        {
            return redirect('org/' . $existedOrgGuid);
        }
        if( !empty($guid) )
        {
            $xmlPath = base_path(TEMPLATES . 'orgs/' . $guid . '.xml');
            $xslPath = base_path(TEMPLATES . 'form.xsl');
        }
        else
        {
            $forms = FormTemplate::where('type', 'org')->first();
            $xmlPath = base_path($forms->template);
            $xslPath = base_path(TEMPLATES . 'form.xsl');
        }

        if( !file_exists($xmlPath) )
        {
            return redirect('org/');
        }

        $xml = new \DOMDocument;
        $xml->load($xmlPath);

        $xsl = new \DOMDocument;
        $xsl->load($xslPath);

        $proc = new \XSLTProcessor;
        $proc->importStyleSheet($xsl); // attach the xsl rules

        $res = $proc->transformToXML($xml);

        return view('forms.org', ['form' => $res, 'guid' => !empty($guid) ? $guid : 'new']);
    }

    private function getValidationData()
    {
        return ['org_type' => 'required',
            'name' => 'required|max:500',
            'address' => "required|max:500",
            'email' => "required|email|max:50",
            'phone' => 'required|max:20',
            'employee_name' => 'required|max:250',
            'employee_position' => 'required|max:100',
            'employee_phone' => 'required|max:20',
            'employee_email' => 'required|email|max:50'
        ];
    }

    private function isGraduateRowExist($guid = null)
    {
        $collection = Organization::where('guid', is_null($guid) ? '' : $guid);
        if( $collection->count() > 0 )
        {
            return true;
        }
        return false;
    }

    public function saveOrgForm(Request $request, $guid = null)
    {
        $forms = FormTemplate::where('type', 'org')->first();
        $xmlPath = base_path($forms->template);

        $isGraduateRowExist = $this->isGraduateRowExist($guid);

        $validator = $this->getValidationFactory()->make($request->all(), $this->getValidationData());
        if ($validator->fails())
        {
            if( strpos($guid, 'tmp-') === false && !$isGraduateRowExist )
            {
                $guid = "tmp-" . md5(time() . $request->name . $request->email . rand(1000, 9999));
            }
            if( !empty($guid) && $isGraduateRowExist)
            {
                $xmlPath = base_path(TEMPLATES . 'orgs/' . $guid . '.xml');
            }
            $this->saveXml($request, $xmlPath, $guid);
            return redirect()->to('org/' . $guid)
                ->withInput($request->input())
                ->withErrors($validator->errors()->getMessages(), $this->errorBag());
        }
        else
        {
            if( strpos($guid, 'tmp-') !== false )
            {
                $tmpXml = base_path(TEMPLATES . 'orgs/' . $guid . '.xml');
                unlink($tmpXml);
            }
        }

        $row = ['org_type' => $request->org_type,
            'name' => $request->name,
            'phone' => $request->phone,
            'address' => $request->address,
            'email' => $request->email,
            'employee_name' => $request->employee_name,
            'employee_phone' => $request->employee_phone,
            'employee_email' => $request->employee_email,
            'employee_position' => $request->employee_position,
            'comment' => $request->comment];

        if( empty($guid) || $guid == 'new' || strpos($guid, 'tmp-') !== false )
        {
            $guid = md5(time() . $request->name . $request->email . rand(1000, 9999));

            $row['form_template_id'] = $forms->id;
            $row['guid'] = $guid;
            $row['filepath'] = TEMPLATES . 'orgs/' . $guid . '.xml';

            Organization::create($row);
            $this->updateUserOrgId($guid);
            $this->saveXml($request, $xmlPath, $guid);
        }
        elseif( !empty($guid) && strlen($guid) >= 32 )
        {
            $collection = Organization::where('guid', $guid);
            if( $collection->count() > 0 )
            {
                $collection->update($row);
                $xmlPath = base_path(TEMPLATES . 'orgs/' . $guid . '.xml');
                $this->saveXml($request, $xmlPath, $guid);
            }
        }

        return redirect('org/' . $guid);
    }

    /**
     * @param $guid
     * @return bool
     *
     * Set form guid for organization account
     */
    protected function updateUserOrgId($guid)
    {
        $user = Auth::user();
        if( $this->isUserRole($user->role_id) )
        {
            User::where('id', $user->id)->update(['org_guid' => $guid]);
            return true;
        }
        return false;
    }

    protected function redirectToExistedOrgIfUserRole($guid = null)
    {
        $user = Auth::user();
        if( $this->isUserRole($user->role_id) )
        {
            $userModel = User::find($user->id);
            if( is_null($guid) || $guid != $userModel->org_guid)
            {
                return $userModel->org_guid;
            }
        }
        return false;
    }

    protected function saveXml(Request $request, $xmlPath, $guid)
    {
        $xml = simplexml_load_file($xmlPath);
        foreach ($request->all() as $key => $value) {
            $type = 'input';
            $obj = $xml->xpath('//input[@field="' . $key . '"]');
            if( (!isset($obj) || empty($obj)) && !is_array($value) )
            {
                $type = 'textarea';
                $obj = $xml->xpath('//textarea[@field="' . $key . '"]');
            }
            if( (!isset($obj) || empty($obj)) && !is_array($value) )
            {
                $type = 'radio';
                $radios = $xml->xpath('//radio[@field="' . $key . '"]');
                for($i = 0; $i < count($radios); $i++)
                {
                    $radios[$i]['checked'] = 'false';
                }
                $obj = $xml->xpath('//radio[@field="' . $key . '"][@value="' . $value . '"]');
            }

            if( isset($obj) && !empty($obj) ) {
                if ($type == 'input' || $type == 'textarea') {
                    $obj[0][0] = $value;
                } elseif($type == 'radio') {
                    $obj[0]['checked'] = 'true';
                }
            }
        }
        $resultXmlPath = base_path(TEMPLATES . 'orgs/' . $guid . '.xml');
        file_put_contents($resultXmlPath, $xml->asXml());

        return $resultXmlPath;
    }
}
