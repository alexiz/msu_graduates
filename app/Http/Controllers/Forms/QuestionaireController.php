<?php

namespace App\Http\Controllers\Forms;

use App\FormTemplate;
use App\Graduate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

const TEMPLATES = 'resources/templates/';

class QuestionaireController extends Controller
{
    public function getStudentFormReadonly($id)
    {
        $graduates = Graduate::find((int)$id);
        if( !isset($graduates->id) )
        {
            abort(404, 'Page not found');
        }


        $xmlPath = base_path(TEMPLATES . 'forms/' . $graduates->guid . '.xml');
        $xslPath = base_path(TEMPLATES . 'form-disabled.xsl');

        if( !file_exists($xmlPath) )
        {
            abort(404, 'Page not found');
        }

        $xml = new \DOMDocument;
        $xml->load($xmlPath);
        $xsl = new \DOMDocument;
        $xsl->load($xslPath);
        $proc = new \XSLTProcessor;
        $proc->importStyleSheet($xsl); // attach the xsl rules
        $res = $proc->transformToXML($xml);

        return view('forms.questionaire-readonly', ['active' => 'questionaire', 'form' => $res, 'id' => (int)$id]);
    }
    
    public function getStudentForm($guid = null)
    {
        if( !empty($guid) && strlen($guid) == 32 )
        {
            $xmlPath = base_path(TEMPLATES . 'forms/' . $guid . '.xml');
            $xslPath = base_path(TEMPLATES . 'form-disabled.xsl');
        }
        elseif(!empty($guid) && strpos($guid, 'tmp-') !== false )
        {
            $xmlPath = base_path(TEMPLATES . 'forms/' . $guid . '.xml');
            $xslPath = base_path(TEMPLATES . 'form.xsl');
        }
        else
        {
            $forms = FormTemplate::where('type', 'main')->first();
            $xmlPath = base_path($forms->template);
            $xslPath = base_path(TEMPLATES . 'form.xsl');
        }

        if( !file_exists($xmlPath) )
        {
            return redirect('questionaire/student/');
        }

        $xml = new \DOMDocument;
        $xml->load($xmlPath);

        $xsl = new \DOMDocument;
        $xsl->load($xslPath);

        $proc = new \XSLTProcessor;
        $proc->importStyleSheet($xsl); // attach the xsl rules

        $res = $proc->transformToXML($xml);

        return view('forms.questionaire', ['active' => 'questionaire', 'form' => $res, 'guid' => !empty($guid) ? $guid : 'new']);
    }

    private function getValidationData($isGraduateRowExist)
    {
        if( $isGraduateRowExist === true )
        {
            return ['group_number' => 'max:20',
                'program' => 'max:500',
                'citizenship' => 'max:500',
                'current_employer_name' => 'max:500',
                'is_accessable_personal_data' => 'required|integer'];
        }
        else
        {
            return ['graduation_year' => 'required|integer',
                'student_id' => 'required|integer',
                'name' => 'required|max:500',
                'email' => "required|email|max:250",
                'phone' => 'required|max:20',
                'group_number' => 'max:20',
                'program' => 'max:500',
                'citizenship' => 'max:500',
                'current_employer_name' => 'max:500',
                'is_accessable_personal_data' => 'required|integer'];
        }
    }

    private function isGraduateRowExist($guid = null)
    {
        $collection = Graduate::where('guid', is_null($guid) ? '' : $guid);
        if( $collection->count() > 0 )
        {
            return true;
        }
        return false;
    }

    public function saveStudentForm(Request $request, $guid = null)
    {
        $forms = FormTemplate::where('type', 'main')->first();
        $xmlPath = base_path($forms->template);

        $isGraduateRowExist = $this->isGraduateRowExist($guid);

        $validator = $this->getValidationFactory()->make($request->all(), $this->getValidationData($isGraduateRowExist));
        if ($validator->fails())
        {
            if( strpos($guid, 'tmp-') === false && !$isGraduateRowExist )
            {
                $guid = "tmp-" . md5(time() . $request->graduation_year . $request->student_id . rand(1000, 9999));
            }
            if( !empty($guid) && $isGraduateRowExist)
            {
                $xmlPath = base_path(TEMPLATES . 'forms/' . $guid . '.xml');
            }
            $this->saveXml($request, $xmlPath, $guid);
            return redirect()->to('questionaire/student/' . $guid)
                            ->withInput($request->input())
                            ->withErrors($validator->errors()->getMessages(), $this->errorBag());
        }
        else
        {
            if( strpos($guid, 'tmp-') !== false )
            {
                $tmpXml = base_path(TEMPLATES . 'forms/' . $guid . '.xml');
                unlink($tmpXml);
                $tmpEmptyCvXml = base_path(TEMPLATES . 'cv/' . $guid . '.xml');
		if( file_exists($tmpEmptyCvXml) ) {
                	unlink($tmpEmptyCvXml);
		}
            }
        }

        $graduate = ['group_number' => $request->group_number,
            'program' => $request->program,
            'citizenship' => $request->citizenship,
            'current_employer_name' => $request->current_employer_name,
            'is_accessable_personal_data' => $request->is_accessable_personal_data];

        if( empty($guid) || $guid == 'new' || strpos($guid, 'tmp-') !== false )
        {
            $guid = md5(time() . $request->graduation_year . $request->student_id . rand(1000, 9999));

            $graduate['graduation_year'] = $request->graduation_year;
            $graduate['student_id'] = $request->student_id;
            $graduate['name'] = $request->name;
            $graduate['email'] = $request->email;
            $graduate['phone'] = $request->phone;
            $graduate['form_template_id'] = $forms->id;
            $graduate['guid'] = $guid;
            $graduate['filepath'] = TEMPLATES . 'forms/' . $guid . '.xml';

            Graduate::create($graduate);
            $this->saveXml($request, $xmlPath, $guid);
            $this->saveEmptyCvXml($request, $guid);
        }
        elseif( !empty($guid) && strlen($guid) >= 32 )
        {
            $collection = Graduate::where('guid', $guid);
            if( $collection->count() > 0 )
            {
                $collection->update($graduate);
                $xmlPath = base_path(TEMPLATES . 'forms/' . $guid . '.xml');
                $this->saveXml($request, $xmlPath, $guid);
            }
        }

        return redirect('questionaire/student/' . $guid);
    }

    protected function saveXml(Request $request, $xmlPath, $guid)
    {
        $xml = simplexml_load_file($xmlPath);
        foreach ($request->all() as $key => $value) {
            $type = 'input';
            $obj = $xml->xpath('//input[@field="' . $key . '"]');
            if( (!isset($obj) || empty($obj)) && !is_array($value) )
            {
                $type = 'radio';
                $radios = $xml->xpath('//radio[@field="' . $key . '"]');
                for($i = 0; $i < count($radios); $i++)
                {
                    $radios[$i]['checked'] = 'false';
                }
                $obj = $xml->xpath('//radio[@field="' . $key . '"][@value="' . $value . '"]');
            }
            if( !isset($obj) || empty($obj) )
            {
                $type = 'checkbox';
                $checkboxes = $xml->xpath('//checkbox[@field="' . $key . '[]"]');
                for($i = 0; $i < count($checkboxes); $i++)
                {
                    $checkboxes[$i]['checked'] = 'false';
                }
                for($i = 0; $i < count($value); $i++)
                {
                    $obj = $xml->xpath('//checkbox[@field="' . $key . '[]"][@value="' . $value[$i] . '"]');
                    if( isset($obj) && !empty($obj) ) {
                        $obj[0]['checked'] = 'true';
                    }
                }
            }

            if( isset($obj) && !empty($obj) ) {
                if ($type == 'input') {
                    $obj[0][0] = $value;
                } elseif($type == 'radio') {
                    $obj[0]['checked'] = 'true';
                }
            }
        }
        $resultXmlPath = base_path(TEMPLATES . 'forms/' . $guid . '.xml');
        file_put_contents($resultXmlPath, $xml->asXml());

        return $resultXmlPath;
    }

    protected function saveEmptyCvXml(Request $request, $guid)
    {
        $forms = FormTemplate::where('type', 'cv')->first();
        $xmlPath = base_path($forms->template);
        $xml = simplexml_load_file($xmlPath);
        foreach ($request->all() as $key => $value) {
            $obj = $xml->xpath('//input[@field="' . $key . '"]');
            if( isset($obj) && !empty($obj) ) {
                $obj[0][0] = $value;
            }
        }
        $resultXmlPath = base_path(TEMPLATES . 'cv/' . $guid . '.xml');
        file_put_contents($resultXmlPath, $xml->asXml());

        return $resultXmlPath;
    }
}
