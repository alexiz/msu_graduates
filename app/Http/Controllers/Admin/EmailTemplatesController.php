<?php

namespace App\Http\Controllers\Admin;

use App\EmailTemplate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmailTemplatesController extends Controller
{

    public function getList()
    {
        $etemplates = EmailTemplate::all();

        return view('admin.email-templates', ['dataset' => $etemplates]);
    }

    public function removeOne(Request $request, $id)
    {
        $item = EmailTemplate::find($id);
        if(isset($item->id))
        {
            $item->delete();
        }
        return redirect('admin/email-templates/list');
    }

    public function editOne(Request $request, $id = null)
    {
        $item = EmailTemplate::find($id);
        return view('admin.email-templates-item', ['item' => $item]);
    }

    public function saveOne(Request $request, $id = null)
    {
        $row = ['body' => $request->body];
        if( is_null($id) )
        {
            EmailTemplate::create($row);
        }
        else
        {
            EmailTemplate::where('id', $id)->update($row);
        }
        return redirect('admin/email-templates/list');
    }
}
