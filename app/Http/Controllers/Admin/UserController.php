<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    public function getList()
    {
        $users = User::all();

        return view('admin.users-list', ['users' => $users]);
    }

    public function blockOne(Request $request, $id)
    {
        $user = User::find($id);
        $status = 0;
        if($user->is_blocked == 0)
        {
            $status = 1;
        }
        User::where('id', $user->id)->update(['is_blocked' => $status]);
        return redirect('admin/users/list');
    }

    public function editOne(Request $request, $id)
    {
        $user = User::find($id);
        $roles = Role::all();
        return view('admin.users-edit', ['user' => $user, 'roles' => $roles]);
    }

    public function saveOne(Request $request, $id)
    {
        $this->validate($request, [
            'id' => 'required|integer',
            'name' => 'required|max:255',
            'email' => "required|email|max:50",
            //'email' => "required|email|unique:users,email,$id|max:50",
        ]);

        User::where('id', $id)->update(['name' => $request->name, 'email' => $request->email, 'role_id' => $request->role_id]);
        return redirect('admin/users/list');
    }

    public function generateResetToken(Request $request, $id)
    {
        $token = str_random(64);
        User::where('id', $id)->update(['remember_token' => $token]);
        return redirect("password/reset/$token");
    }

    public function changePasswordForm(Request $request, $token)
    {
        return view('auth.passwords.reset', ['token' => $token]);
    }

    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ]);

        User::where('remember_token', $request->token)->update(['remember_token' => '', 'password' => bcrypt($request->password)]);
        return redirect("admin/users/list");
    }
}
