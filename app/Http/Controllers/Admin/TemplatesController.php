<?php

namespace App\Http\Controllers\Admin;

use App\FormTemplate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TemplatesController extends Controller
{

    public function getList()
    {
        $templates = FormTemplate::all();

        return view('admin.form-templates', ['templates' => $templates]);
    }
}
