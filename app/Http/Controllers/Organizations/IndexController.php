<?php

namespace App\Http\Controllers\Organizations;

use App\Organization;
use App\Http\Traits;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    use Traits\RoleTrait;
    private function getRussianLabelForTypes($type = null, $getAll = false)
    {
        $labels = ['law_world_company' => ['short' => 'Международ. юр. фирма', 'long' => 'Международная юридическая фирма'],
                    'law_russian_company' => ['short' => 'Юр. фирма РФ', 'long' => 'Российская юридическая фирма'],
                    'auditing_company' => ['short' => 'Аудит. компания', 'long' => 'Аудиторская фирма'],
                    'com_company' => ['short' => 'Коммерч. организация', 'long' => 'Коммерческая организация'],
                    'gov_company' => ['short' => 'Гос. организация', 'long' => 'Государственная организация']];
        if( $getAll )
        {
            return $labels;
        }
        if( !isset($labels[$type]) )
        {
            return null;
        }
        return $labels[$type];
    }

    public function getList($type = null, $page = 1, $sort = 'name', $direction = 'asc')
    {
        $types = Organization::orgTypes()->get();
        $limit = 50;
        $orgs = Organization::getList($type, $page, $limit, $sort, $direction)->get();

        $user = Auth::user();
        $readonly = '';
        if( $this->isUserRole($user->role_id) )
        {
            $readonly = '-readonly';
        }
        return view('organizations.organizations' . $readonly, ['types' => $types,
                                                    'labels' => $this->getRussianLabelForTypes(null, true),
                                                    'orgs' => $orgs,
                                                    'org_type_title' => $this->getRussianLabelForTypes($type),
                                                    'limit' => $limit]);
    }

    public function deleteItem($guid)
    {
        $org = Organization::where('guid', $guid)->first();
        if( isset($org->filepath) ) {
            $newFilename = str_replace('.xml', '-removed.xml', $org->filepath);
            rename(base_path($org->filepath), base_path($newFilename));
            $date = new \DateTime();
            Organization::where('guid', $org->guid)->update(['deleted_at' => $date->format('Y-m-d H:i:s'), 'filepath' => $newFilename]);
        }
        return redirect(request()->headers->get('referer'));
    }
}
