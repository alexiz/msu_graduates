<?php

namespace App\Http\Controllers\Home;

use App\Graduate;
use App\Organization;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Traits;
use Illuminate\Support\Facades\Auth;

class SearchController extends Controller
{
    use Traits\RoleTrait;
    public function search(Request $request)
    {
        $user = Auth::user();
        $readonly = false;
        if( $this->isUserRole($user->role_id) )
        {
            $readonly = true;
        }

        $query = $request->get('query');

        $results = [];
        if (!empty($query))
        {
            $cl = new \SphinxClient();
            $cl->SetServer('127.0.0.1', 9312);
            $cl->SetLimits(0, 50);
            $cl->SetMatchMode(SPH_MATCH_ANY);
            $response = $cl->Query($query, 'law_msu_index');
            if( isset($response['matches']) && !empty($response['matches']))
            {
                foreach ($response['matches'] as $key => $value)
                {
                    $results[] = $this->decorateSphinxResponse($key, $value['attrs']['type'], $readonly);
                }
            }
        }
        if ($request->ajax()) {
            return $results;
        }

        $query = stripslashes($query);
        if( $readonly )
        {
            return view('home.search-readonly', compact('results','query'));
        }
        else
        {
            return view('home.search', compact('results','query'));
        }
    }

    private function decorateSphinxResponse($id, $type, $readonly)
    {
        $response = ['id' => $id];
        if( $type == 'graduate' )
        {
            $row = Graduate::find($id);
            $response['name'] = 'Выпускник ' . $row['graduation_year'] . ': ' . $row['name'];
            $response['email'] = 'Email: ' . $row['email'];
            $response['phone'] = 'Тел: ' . $row['phone'];
            $response['guid'] = $row['guid'];
            $response['fld'] = 'Текущее место работы: ' . (empty($row['current_employer_name']) ? 'отсутствует' : $row['current_employer_name']);
            if( $readonly )
            {
                $response['url'] = '/student/form/' . $row['id'];
            }
            else
            {
                $response['url'] = '/questionaire/student/' . $row['guid'];
            }
        }
        if( $type == 'organization' && !$readonly )
        {
            $row = Organization::find($id);
            $response['name'] = 'Органзиация: ' . $row['name'];
            $response['email'] = 'Email: ' . $row['email'];
            $response['phone'] = 'Тел: ' . $row['phone'];
            $response['guid'] = $row['guid'];
            $response['fld'] = 'Адрес: ' . $row['address'] . '; Сотрудник: ' . $row['employee_name'] . ' ' . $row['employee_phone'];
            //if( $readonly )
            //{
            //    $response['url'] = '/company/' . $row['id'];
            //}
            //else
            //{
                $response['url'] = '/org/' . $row['guid'];
            //}
        }

        return $response;
    }

    public function searchCustom(Request $request)
    {
        return view('home.search-custom', compact('results','query'));
    }
}
