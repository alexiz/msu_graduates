<?php
namespace App\Http\Traits;


trait RoleTrait {
    static $student_role = 1;
    static $user_role = 2;
    static $manager_role = 3;
    static $admin_role = 4;

    public function isUserRole($role_id) {
        if( $role_id == self::$user_role )
        {
            return true;
        }
        return false;
    }

    public function isManagerRole($role_id) {
        if( $role_id == self::$manager_role )
        {
            return true;
        }
        return false;
    }
}