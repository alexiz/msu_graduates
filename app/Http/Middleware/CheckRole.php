<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRole
{

    private $roles = [1 => 'student', 2 => 'user', 3 => 'manager', 4 => 'admin'];
   
    private function getRoleName($role_id)
    {
        return $this->roles[$role_id];
    }
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        $roles = explode('_', $role);
        $user = Auth::user();

        $abort = true;

        foreach($roles as $k => $roleName)
        {
            if ( $this->getRoleName($user->role_id) == $roleName ) {
                $abort = false;
                break;
            }
        }

        if( $abort )
        {
            abort(404, 'Page not found');
        }

        return $next($request);
    }

}