<?php

namespace App\Console\Commands;

use App\Delivery;
use App\EmailTemplate;
use Illuminate\Console\Command;
use Mail;
use App\DeliveryQueue;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send emails from queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $list = DeliveryQueue::where('pause', 0)->get();
        foreach($list as $item)
        {
            $delivery = Delivery::where('id', $item->delivery_id)->first();
            $emailTemplate = EmailTemplate::where('id', $delivery->email_template_id)->first();
            $data = ['item' => $item, 'delivery' => $delivery];
            $emailBody = str_replace('{FULL_NAME}', $item->recepient_name, $emailTemplate->body);
            $emailBody = str_replace('{EMAIL}', $item->recepient_email, $emailBody);
            if( isset($item->form_link) && !empty($item->form_link) )
            {
                $emailBody = str_replace('{FORM_LINK}', $item->form_link, $emailBody);
            }
            Mail::send('emails.empty',['template' => $emailBody], function($message) use ($data) {
                $message->from('career.law.msu@gmail.com', 'Центра карьеры Юрфака МГУ');
                $message->to($data['item']->recepient_email, $data['item']->recepient_name)->subject($data['delivery']->subject);
            });
            $this->line('delivery_id: ' . $item->delivery_id . ' sent to: ' . $item->recepient_email);
            DeliveryQueue::where('recepient_email', $item->recepient_email)->where('job_id', $item->job_id)->delete();
            sleep(3);
        }

    }
}
