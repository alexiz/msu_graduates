<?php

namespace App\Console\Commands;

use App\Graduate;
use Illuminate\Console\Command;

const TEMPLATES = 'resources/templates/';

class GenerateForms extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'form:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate forms for empty graduates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $list = Graduate::where('guid', '00000000000000000000000000000000')->get();
        $base = base_path(TEMPLATES .'form.xml');
	$basecv = base_path(TEMPLATES . 'cv.xml');
        foreach($list as $item)
        {
            $guid = md5($item->name . time() . rand(0,9999));
            $new = base_path(TEMPLATES . 'forms/' . $guid . '.xml');
	    $newcv = base_path(TEMPLATES . 'cv/' . $guid . '.xml');
	    
	    $xml = simplexml_load_file($base);
            $node1 = $xml->xpath('//input[@field="graduation_year"]');
            $node1[0][0] = $item->graduation_year;
	    $node2 = $xml->xpath('//input[@field="student_id"]');
            $node2[0][0] = $item->student_id;
            $node3 = $xml->xpath('//input[@field="name"]');
            $node3[0][0] = $item->name;
            $node4 = $xml->xpath('//input[@field="phone"]');
            $node4[0][0] = $item->phone;
	    $node5 = $xml->xpath('//input[@field="email"]');
            $node5[0][0] = $item->email;
	    file_put_contents($new, $xml->asXml());
	
	    $xmlcv = simplexml_load_file($basecv);
	    $cv_node1 = $xmlcv->xpath('//input[@field="name"]');
            $cv_node1[0][0] = $item->name;
            $cv_node2 = $xmlcv->xpath('//input[@field="email"]');
            $cv_node2[0][0] = $item->email;
            $cv_node3 = $xmlcv->xpath('//input[@field="phone"]');
            $cv_node3[0][0] = $item->phone;
	    file_put_contents($newcv, $xmlcv->asXml());
	

           
            Graduate::where('id', $item->id)->update(['guid' => $guid, 'filepath' => TEMPLATES . 'forms/' . $guid . '.xml']);
            $this->line('row ' . $item->id . ' is updated');
        }

    }
}
