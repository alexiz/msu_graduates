<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{

    protected $types = ['law_world_company' => 'Международная юридическая фирма',
                            'law_russian_company' => 'Российская юридическая фирма',
                            'auditing_company' => 'Аудиторская компания',
                            'com_company' => 'Коммерческая организация',
                            'gov_company' => 'Государственная организация'];

    protected $fillable = ['org_type', 'name', 'email', 'phone', 'address',
        'employee_name', 'employee_phone', 'employee_email', 'employee_position',
        'form_template_id', 'guid', 'filepath'];

    public function scopeOrgTypes($query)
    {
        return $this->select('org_type', DB::raw('COUNT(org_type) as total'))
            ->whereNull('deleted_at')
            ->groupBy('org_type')
            ->orderBy('org_type', 'desc');
    }

    protected function getPreparedParams($fields, $page = 1, $limit = 50, $sort = null, $direction = 'asc')
    {
        if( (int)$page < 1 || (int)$page > 9999999 )
        {
            $page = 1;
        }

        if( is_null($sort) || !in_array($sort, $fields) )
        {
            $sort = $fields[0];
        }

        if( !in_array($direction, ['asc', 'desc']) )
        {
            $direction = 'asc';
        }

        return ['offset' => ($page - 1) * $limit, 'limit' => $limit, 'sort' => $sort, 'direction' => $direction];
    }

    public function scopeGetList($query, $type, $page, $limit, $sort, $direction)
    {
        $fields = ['id','org_type',
                    'name',
                    'phone',
                    'email',
                    'address',
                    'employee_name',
                    'employee_email',
                    'employee_phone',
                    'guid'];

        $params = $this->getPreparedParams($fields, $page, $limit, $sort, $direction);


        $q = $this->select($fields)
            ->whereNull('deleted_at')
            ->orderBy($params['sort'], $params['direction'])
            ->offset($params['offset'])
            ->limit($params['limit']);

        if( isset($this->types[$type]))
        {
            $q->where('org_type', $type);
        }
        //var_dump($q->toSql());exit();

        return $q;
    }

    public function scopeGetEmailPools()
    {
        return $this->select(DB::raw('org_type as name'), DB::raw('COUNT(org_type) as total'), DB::raw("'org' as type"), DB::raw("'Организации' as type2"))
            ->whereNull('deleted_at')
            ->groupBy('org_type');
    }

    public function scopeGetEmails($query, $type, $org_type, $is_accessable_personal_data = false)
    {
        if( $type != 'org' )
        {
            return $this->select(DB::raw('employee_email as email'), DB::raw('employee_name as name'), DB::raw('concat(\'http://careerlawmsu.ru/org\', guid) as form_link') )->where('id', 0);
        }
        return $this->select(DB::raw('employee_email as email'), DB::raw('employee_name as name'), DB::raw('concat(\'http://careerlawmsu.ru/org\', guid) as form_link'))
            ->whereNull('deleted_at')
            ->where('org_type', $org_type);
    }
}
