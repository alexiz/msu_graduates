<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryQueue extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'job_id', 'delivery_id', 'pause', 'is_busy', 'recepient_email', 'recepient_name', 'form_link'
    ];

    public function scopeGetMaxJobId()
    {
        return $this->select('*')
            ->orderBy('job_id', 'desc')
            ->first();
    }
}
